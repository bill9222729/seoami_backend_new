<?php namespace Tests\Repositories;

use App\Models\wallet_log;
use App\Repositories\wallet_logRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class wallet_logRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var wallet_logRepository
     */
    protected $walletLogRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->walletLogRepo = \App::make(wallet_logRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_wallet_log()
    {
        $walletLog = wallet_log::factory()->make()->toArray();

        $createdwallet_log = $this->walletLogRepo->create($walletLog);

        $createdwallet_log = $createdwallet_log->toArray();
        $this->assertArrayHasKey('id', $createdwallet_log);
        $this->assertNotNull($createdwallet_log['id'], 'Created wallet_log must have id specified');
        $this->assertNotNull(wallet_log::find($createdwallet_log['id']), 'wallet_log with given id must be in DB');
        $this->assertModelData($walletLog, $createdwallet_log);
    }

    /**
     * @test read
     */
    public function test_read_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();

        $dbwallet_log = $this->walletLogRepo->find($walletLog->id);

        $dbwallet_log = $dbwallet_log->toArray();
        $this->assertModelData($walletLog->toArray(), $dbwallet_log);
    }

    /**
     * @test update
     */
    public function test_update_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();
        $fakewallet_log = wallet_log::factory()->make()->toArray();

        $updatedwallet_log = $this->walletLogRepo->update($fakewallet_log, $walletLog->id);

        $this->assertModelData($fakewallet_log, $updatedwallet_log->toArray());
        $dbwallet_log = $this->walletLogRepo->find($walletLog->id);
        $this->assertModelData($fakewallet_log, $dbwallet_log->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();

        $resp = $this->walletLogRepo->delete($walletLog->id);

        $this->assertTrue($resp);
        $this->assertNull(wallet_log::find($walletLog->id), 'wallet_log should not exist in DB');
    }
}
