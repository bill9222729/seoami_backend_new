<?php namespace Tests\Repositories;

use App\Models\trans_to_bank;
use App\Repositories\trans_to_bankRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class trans_to_bankRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var trans_to_bankRepository
     */
    protected $transToBankRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->transToBankRepo = \App::make(trans_to_bankRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->make()->toArray();

        $createdtrans_to_bank = $this->transToBankRepo->create($transToBank);

        $createdtrans_to_bank = $createdtrans_to_bank->toArray();
        $this->assertArrayHasKey('id', $createdtrans_to_bank);
        $this->assertNotNull($createdtrans_to_bank['id'], 'Created trans_to_bank must have id specified');
        $this->assertNotNull(trans_to_bank::find($createdtrans_to_bank['id']), 'trans_to_bank with given id must be in DB');
        $this->assertModelData($transToBank, $createdtrans_to_bank);
    }

    /**
     * @test read
     */
    public function test_read_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();

        $dbtrans_to_bank = $this->transToBankRepo->find($transToBank->id);

        $dbtrans_to_bank = $dbtrans_to_bank->toArray();
        $this->assertModelData($transToBank->toArray(), $dbtrans_to_bank);
    }

    /**
     * @test update
     */
    public function test_update_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();
        $faketrans_to_bank = trans_to_bank::factory()->make()->toArray();

        $updatedtrans_to_bank = $this->transToBankRepo->update($faketrans_to_bank, $transToBank->id);

        $this->assertModelData($faketrans_to_bank, $updatedtrans_to_bank->toArray());
        $dbtrans_to_bank = $this->transToBankRepo->find($transToBank->id);
        $this->assertModelData($faketrans_to_bank, $dbtrans_to_bank->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();

        $resp = $this->transToBankRepo->delete($transToBank->id);

        $this->assertTrue($resp);
        $this->assertNull(trans_to_bank::find($transToBank->id), 'trans_to_bank should not exist in DB');
    }
}
