<?php namespace Tests\Repositories;

use App\Models\wallet;
use App\Repositories\walletRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class walletRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var walletRepository
     */
    protected $walletRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->walletRepo = \App::make(walletRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_wallet()
    {
        $wallet = wallet::factory()->make()->toArray();

        $createdwallet = $this->walletRepo->create($wallet);

        $createdwallet = $createdwallet->toArray();
        $this->assertArrayHasKey('id', $createdwallet);
        $this->assertNotNull($createdwallet['id'], 'Created wallet must have id specified');
        $this->assertNotNull(wallet::find($createdwallet['id']), 'wallet with given id must be in DB');
        $this->assertModelData($wallet, $createdwallet);
    }

    /**
     * @test read
     */
    public function test_read_wallet()
    {
        $wallet = wallet::factory()->create();

        $dbwallet = $this->walletRepo->find($wallet->id);

        $dbwallet = $dbwallet->toArray();
        $this->assertModelData($wallet->toArray(), $dbwallet);
    }

    /**
     * @test update
     */
    public function test_update_wallet()
    {
        $wallet = wallet::factory()->create();
        $fakewallet = wallet::factory()->make()->toArray();

        $updatedwallet = $this->walletRepo->update($fakewallet, $wallet->id);

        $this->assertModelData($fakewallet, $updatedwallet->toArray());
        $dbwallet = $this->walletRepo->find($wallet->id);
        $this->assertModelData($fakewallet, $dbwallet->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wallet()
    {
        $wallet = wallet::factory()->create();

        $resp = $this->walletRepo->delete($wallet->id);

        $this->assertTrue($resp);
        $this->assertNull(wallet::find($wallet->id), 'wallet should not exist in DB');
    }
}
