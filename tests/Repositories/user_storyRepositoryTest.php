<?php namespace Tests\Repositories;

use App\Models\user_story;
use App\Repositories\user_storyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class user_storyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var user_storyRepository
     */
    protected $userStoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userStoryRepo = \App::make(user_storyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user_story()
    {
        $userStory = user_story::factory()->make()->toArray();

        $createduser_story = $this->userStoryRepo->create($userStory);

        $createduser_story = $createduser_story->toArray();
        $this->assertArrayHasKey('id', $createduser_story);
        $this->assertNotNull($createduser_story['id'], 'Created user_story must have id specified');
        $this->assertNotNull(user_story::find($createduser_story['id']), 'user_story with given id must be in DB');
        $this->assertModelData($userStory, $createduser_story);
    }

    /**
     * @test read
     */
    public function test_read_user_story()
    {
        $userStory = user_story::factory()->create();

        $dbuser_story = $this->userStoryRepo->find($userStory->id);

        $dbuser_story = $dbuser_story->toArray();
        $this->assertModelData($userStory->toArray(), $dbuser_story);
    }

    /**
     * @test update
     */
    public function test_update_user_story()
    {
        $userStory = user_story::factory()->create();
        $fakeuser_story = user_story::factory()->make()->toArray();

        $updateduser_story = $this->userStoryRepo->update($fakeuser_story, $userStory->id);

        $this->assertModelData($fakeuser_story, $updateduser_story->toArray());
        $dbuser_story = $this->userStoryRepo->find($userStory->id);
        $this->assertModelData($fakeuser_story, $dbuser_story->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user_story()
    {
        $userStory = user_story::factory()->create();

        $resp = $this->userStoryRepo->delete($userStory->id);

        $this->assertTrue($resp);
        $this->assertNull(user_story::find($userStory->id), 'user_story should not exist in DB');
    }
}
