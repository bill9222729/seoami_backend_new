<?php namespace Tests\Repositories;

use App\Models\slider_manager;
use App\Repositories\slider_managerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class slider_managerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var slider_managerRepository
     */
    protected $sliderManagerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sliderManagerRepo = \App::make(slider_managerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_slider_manager()
    {
        $sliderManager = slider_manager::factory()->make()->toArray();

        $createdslider_manager = $this->sliderManagerRepo->create($sliderManager);

        $createdslider_manager = $createdslider_manager->toArray();
        $this->assertArrayHasKey('id', $createdslider_manager);
        $this->assertNotNull($createdslider_manager['id'], 'Created slider_manager must have id specified');
        $this->assertNotNull(slider_manager::find($createdslider_manager['id']), 'slider_manager with given id must be in DB');
        $this->assertModelData($sliderManager, $createdslider_manager);
    }

    /**
     * @test read
     */
    public function test_read_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();

        $dbslider_manager = $this->sliderManagerRepo->find($sliderManager->id);

        $dbslider_manager = $dbslider_manager->toArray();
        $this->assertModelData($sliderManager->toArray(), $dbslider_manager);
    }

    /**
     * @test update
     */
    public function test_update_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();
        $fakeslider_manager = slider_manager::factory()->make()->toArray();

        $updatedslider_manager = $this->sliderManagerRepo->update($fakeslider_manager, $sliderManager->id);

        $this->assertModelData($fakeslider_manager, $updatedslider_manager->toArray());
        $dbslider_manager = $this->sliderManagerRepo->find($sliderManager->id);
        $this->assertModelData($fakeslider_manager, $dbslider_manager->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();

        $resp = $this->sliderManagerRepo->delete($sliderManager->id);

        $this->assertTrue($resp);
        $this->assertNull(slider_manager::find($sliderManager->id), 'slider_manager should not exist in DB');
    }
}
