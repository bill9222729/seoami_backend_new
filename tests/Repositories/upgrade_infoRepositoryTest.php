<?php namespace Tests\Repositories;

use App\Models\upgrade_info;
use App\Repositories\upgrade_infoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class upgrade_infoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var upgrade_infoRepository
     */
    protected $upgradeInfoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->upgradeInfoRepo = \App::make(upgrade_infoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->make()->toArray();

        $createdupgrade_info = $this->upgradeInfoRepo->create($upgradeInfo);

        $createdupgrade_info = $createdupgrade_info->toArray();
        $this->assertArrayHasKey('id', $createdupgrade_info);
        $this->assertNotNull($createdupgrade_info['id'], 'Created upgrade_info must have id specified');
        $this->assertNotNull(upgrade_info::find($createdupgrade_info['id']), 'upgrade_info with given id must be in DB');
        $this->assertModelData($upgradeInfo, $createdupgrade_info);
    }

    /**
     * @test read
     */
    public function test_read_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();

        $dbupgrade_info = $this->upgradeInfoRepo->find($upgradeInfo->id);

        $dbupgrade_info = $dbupgrade_info->toArray();
        $this->assertModelData($upgradeInfo->toArray(), $dbupgrade_info);
    }

    /**
     * @test update
     */
    public function test_update_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();
        $fakeupgrade_info = upgrade_info::factory()->make()->toArray();

        $updatedupgrade_info = $this->upgradeInfoRepo->update($fakeupgrade_info, $upgradeInfo->id);

        $this->assertModelData($fakeupgrade_info, $updatedupgrade_info->toArray());
        $dbupgrade_info = $this->upgradeInfoRepo->find($upgradeInfo->id);
        $this->assertModelData($fakeupgrade_info, $dbupgrade_info->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();

        $resp = $this->upgradeInfoRepo->delete($upgradeInfo->id);

        $this->assertTrue($resp);
        $this->assertNull(upgrade_info::find($upgradeInfo->id), 'upgrade_info should not exist in DB');
    }
}
