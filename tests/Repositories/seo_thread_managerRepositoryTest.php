<?php namespace Tests\Repositories;

use App\Models\seo_thread_manager;
use App\Repositories\seo_thread_managerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class seo_thread_managerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var seo_thread_managerRepository
     */
    protected $seoThreadManagerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seoThreadManagerRepo = \App::make(seo_thread_managerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->make()->toArray();

        $createdseo_thread_manager = $this->seoThreadManagerRepo->create($seoThreadManager);

        $createdseo_thread_manager = $createdseo_thread_manager->toArray();
        $this->assertArrayHasKey('id', $createdseo_thread_manager);
        $this->assertNotNull($createdseo_thread_manager['id'], 'Created seo_thread_manager must have id specified');
        $this->assertNotNull(seo_thread_manager::find($createdseo_thread_manager['id']), 'seo_thread_manager with given id must be in DB');
        $this->assertModelData($seoThreadManager, $createdseo_thread_manager);
    }

    /**
     * @test read
     */
    public function test_read_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();

        $dbseo_thread_manager = $this->seoThreadManagerRepo->find($seoThreadManager->id);

        $dbseo_thread_manager = $dbseo_thread_manager->toArray();
        $this->assertModelData($seoThreadManager->toArray(), $dbseo_thread_manager);
    }

    /**
     * @test update
     */
    public function test_update_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();
        $fakeseo_thread_manager = seo_thread_manager::factory()->make()->toArray();

        $updatedseo_thread_manager = $this->seoThreadManagerRepo->update($fakeseo_thread_manager, $seoThreadManager->id);

        $this->assertModelData($fakeseo_thread_manager, $updatedseo_thread_manager->toArray());
        $dbseo_thread_manager = $this->seoThreadManagerRepo->find($seoThreadManager->id);
        $this->assertModelData($fakeseo_thread_manager, $dbseo_thread_manager->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();

        $resp = $this->seoThreadManagerRepo->delete($seoThreadManager->id);

        $this->assertTrue($resp);
        $this->assertNull(seo_thread_manager::find($seoThreadManager->id), 'seo_thread_manager should not exist in DB');
    }
}
