<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\user_story;

class user_storyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_user_story()
    {
        $userStory = user_story::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/user_stories', $userStory
        );

        $this->assertApiResponse($userStory);
    }

    /**
     * @test
     */
    public function test_read_user_story()
    {
        $userStory = user_story::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/user_stories/'.$userStory->id
        );

        $this->assertApiResponse($userStory->toArray());
    }

    /**
     * @test
     */
    public function test_update_user_story()
    {
        $userStory = user_story::factory()->create();
        $editeduser_story = user_story::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/user_stories/'.$userStory->id,
            $editeduser_story
        );

        $this->assertApiResponse($editeduser_story);
    }

    /**
     * @test
     */
    public function test_delete_user_story()
    {
        $userStory = user_story::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/user_stories/'.$userStory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/user_stories/'.$userStory->id
        );

        $this->response->assertStatus(404);
    }
}
