<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\slider_manager;

class slider_managerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_slider_manager()
    {
        $sliderManager = slider_manager::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/slider_managers', $sliderManager
        );

        $this->assertApiResponse($sliderManager);
    }

    /**
     * @test
     */
    public function test_read_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/slider_managers/'.$sliderManager->id
        );

        $this->assertApiResponse($sliderManager->toArray());
    }

    /**
     * @test
     */
    public function test_update_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();
        $editedslider_manager = slider_manager::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/slider_managers/'.$sliderManager->id,
            $editedslider_manager
        );

        $this->assertApiResponse($editedslider_manager);
    }

    /**
     * @test
     */
    public function test_delete_slider_manager()
    {
        $sliderManager = slider_manager::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/slider_managers/'.$sliderManager->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/slider_managers/'.$sliderManager->id
        );

        $this->response->assertStatus(404);
    }
}
