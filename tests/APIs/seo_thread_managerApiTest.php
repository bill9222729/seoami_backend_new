<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\seo_thread_manager;

class seo_thread_managerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seo_thread_managers', $seoThreadManager
        );

        $this->assertApiResponse($seoThreadManager);
    }

    /**
     * @test
     */
    public function test_read_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/seo_thread_managers/'.$seoThreadManager->id
        );

        $this->assertApiResponse($seoThreadManager->toArray());
    }

    /**
     * @test
     */
    public function test_update_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();
        $editedseo_thread_manager = seo_thread_manager::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seo_thread_managers/'.$seoThreadManager->id,
            $editedseo_thread_manager
        );

        $this->assertApiResponse($editedseo_thread_manager);
    }

    /**
     * @test
     */
    public function test_delete_seo_thread_manager()
    {
        $seoThreadManager = seo_thread_manager::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seo_thread_managers/'.$seoThreadManager->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seo_thread_managers/'.$seoThreadManager->id
        );

        $this->response->assertStatus(404);
    }
}
