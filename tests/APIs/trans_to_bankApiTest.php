<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\trans_to_bank;

class trans_to_bankApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/trans_to_banks', $transToBank
        );

        $this->assertApiResponse($transToBank);
    }

    /**
     * @test
     */
    public function test_read_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/trans_to_banks/'.$transToBank->id
        );

        $this->assertApiResponse($transToBank->toArray());
    }

    /**
     * @test
     */
    public function test_update_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();
        $editedtrans_to_bank = trans_to_bank::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/trans_to_banks/'.$transToBank->id,
            $editedtrans_to_bank
        );

        $this->assertApiResponse($editedtrans_to_bank);
    }

    /**
     * @test
     */
    public function test_delete_trans_to_bank()
    {
        $transToBank = trans_to_bank::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/trans_to_banks/'.$transToBank->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/trans_to_banks/'.$transToBank->id
        );

        $this->response->assertStatus(404);
    }
}
