<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\upgrade_info;

class upgrade_infoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/upgrade_infos', $upgradeInfo
        );

        $this->assertApiResponse($upgradeInfo);
    }

    /**
     * @test
     */
    public function test_read_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/upgrade_infos/'.$upgradeInfo->id
        );

        $this->assertApiResponse($upgradeInfo->toArray());
    }

    /**
     * @test
     */
    public function test_update_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();
        $editedupgrade_info = upgrade_info::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/upgrade_infos/'.$upgradeInfo->id,
            $editedupgrade_info
        );

        $this->assertApiResponse($editedupgrade_info);
    }

    /**
     * @test
     */
    public function test_delete_upgrade_info()
    {
        $upgradeInfo = upgrade_info::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/upgrade_infos/'.$upgradeInfo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/upgrade_infos/'.$upgradeInfo->id
        );

        $this->response->assertStatus(404);
    }
}
