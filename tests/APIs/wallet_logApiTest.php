<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\wallet_log;

class wallet_logApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_wallet_log()
    {
        $walletLog = wallet_log::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/wallet_logs', $walletLog
        );

        $this->assertApiResponse($walletLog);
    }

    /**
     * @test
     */
    public function test_read_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/wallet_logs/'.$walletLog->id
        );

        $this->assertApiResponse($walletLog->toArray());
    }

    /**
     * @test
     */
    public function test_update_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();
        $editedwallet_log = wallet_log::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/wallet_logs/'.$walletLog->id,
            $editedwallet_log
        );

        $this->assertApiResponse($editedwallet_log);
    }

    /**
     * @test
     */
    public function test_delete_wallet_log()
    {
        $walletLog = wallet_log::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/wallet_logs/'.$walletLog->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/wallet_logs/'.$walletLog->id
        );

        $this->response->assertStatus(404);
    }
}
