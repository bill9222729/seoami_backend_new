<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- From Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_id', 'From Id:') !!}
    {!! Form::number('from_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Reason Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason', 'Reason:') !!}
    {!! Form::text('reason', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('point', 'Point:') !!}
    {!! Form::number('point', null, ['class' => 'form-control']) !!}
</div>

<!-- Before Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('before_point', 'Before Point:') !!}
    {!! Form::number('before_point', null, ['class' => 'form-control']) !!}
</div>

<!-- After Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('after_point', 'After Point:') !!}
    {!! Form::number('after_point', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('walletLogs.index') }}" class="btn btn-secondary">Cancel</a>
</div>
