<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $walletLog->user_id }}</p>
</div>

<!-- From Id Field -->
<div class="form-group">
    {!! Form::label('from_id', 'From Id:') !!}
    <p>{{ $walletLog->from_id }}</p>
</div>

<!-- Reason Field -->
<div class="form-group">
    {!! Form::label('reason', 'Reason:') !!}
    <p>{{ $walletLog->reason }}</p>
</div>

<!-- Point Field -->
<div class="form-group">
    {!! Form::label('point', 'Point:') !!}
    <p>{{ $walletLog->point }}</p>
</div>

<!-- Before Point Field -->
<div class="form-group">
    {!! Form::label('before_point', 'Before Point:') !!}
    <p>{{ $walletLog->before_point }}</p>
</div>

<!-- After Point Field -->
<div class="form-group">
    {!! Form::label('after_point', 'After Point:') !!}
    <p>{{ $walletLog->after_point }}</p>
</div>

