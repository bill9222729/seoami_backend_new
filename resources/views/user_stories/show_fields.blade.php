<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $userStory->user_id }}</p>
</div>

<!-- Content Type Field -->
<div class="form-group">
    {!! Form::label('content_type', 'Content Type:') !!}
    <p>{{ $userStory->content_type }}</p>
</div>

<!-- Data Field -->
<div class="form-group">
    {!! Form::label('data', 'Data:') !!}
    <p>{{ $userStory->data }}</p>
</div>

<!-- Background Field -->
<div class="form-group">
    {!! Form::label('background', 'Background:') !!}
    <p>{{ $userStory->background }}</p>
</div>

