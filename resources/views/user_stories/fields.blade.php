<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('content_type', 'Content Type:') !!}
    {!! Form::text('content_type', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Data Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::textarea('data', null, ['class' => 'form-control']) !!}
    
  
</div>
@if(isset($userStory))
<div class="form-group col-sm-6 col-lg-6">
    
    
   
    <img src="{{$userStory->data}}" width="100%" height="100%" alt=""/>
   
</div>
@endif
<!-- Background Field -->
<div class="form-group col-sm-6">
    {!! Form::label('background', 'Background:') !!}
    {!! Form::text('background', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('userStories.index') }}" class="btn btn-secondary">Cancel</a>
</div>
