<!-- Company Name Field -->
<div class="form-group">
    {!! Form::label('company_name', 'Company Name:') !!}
    <p>{{ $upgradeInfo->company_name }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $upgradeInfo->address }}</p>
</div>

<!-- Contact Name Field -->
<div class="form-group">
    {!! Form::label('contact_name', 'Contact Name:') !!}
    <p>{{ $upgradeInfo->contact_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $upgradeInfo->email }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $upgradeInfo->phone }}</p>
</div>

<!-- Ref Id Field -->
<div class="form-group">
    {!! Form::label('ref_id', 'Ref Id:') !!}
    <p>{{ $upgradeInfo->ref_id }}</p>
</div>

<!-- Place Id Field -->
<div class="form-group">
    {!! Form::label('place_id', 'Place Id:') !!}
    <p>{{ $upgradeInfo->place_id }}</p>
</div>

