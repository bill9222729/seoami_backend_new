@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@push('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}


<script>
    function Change_User_Stat(uid,name,stat_lv){

        var lv_name = "認証失敗";
        var stat_color = "danger";
        switch (stat_lv) {
            case 1:
                lv_name= "白金"
                stat_color = "success";
                break;

            case 2:
            lv_name= "鑽石"
            stat_color = "success";
            break;
        
            default:
                break;
        }
        var lv_html  =`<label class="btn btn-sm btn-${stat_color}  add-group-member">${ lv_name }</label>`

        swal.fire({
            title: `你確定要更改用戶 ${name} 的等級 ?`,
            html: `認証等級將會改為 ${lv_html}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
        }).then((result) => {
            var post_data = {
                status:stat_lv
            }

            $.ajax({
            url: "/api/upgrade_infos/"+uid,
            type: 'PUT',
            data: post_data,
            success: function (result) {
                LaravelDataTables.dataTableBuilder.ajax.reload();
                swal.fire({
            title: `更新成功`,
            icon: 'success',
            
            confirmButtonText: 'OK'});


            
            },
            error: function (result) {
               
            },
        });
        
        });
    }
    function status_render(data,type,full,meta){
        // let checked = row.email_verified_at === null ? '' : 'checked disabled';
        var stat_name = "未認証";
        var stat_color = "info";
        if(data == 1){
            stat_name = "白金";
            stat_color = "success";
        }
        if(data == 2){
            stat_name = "鑽石";
            stat_color = "primary";
        }
        if(data == -1){
            stat_name = "認証失敗"
            stat_color = "danger"
        }

        var label = `<label class="btn btn-sm btn-${stat_color} float-right add-group-member">${ stat_name }</label>`
        var upgrade_btn = `<button class="btn btn-sm btn-danger" onclick="Change_User_Stat(${full.id},'${full.contact_name}',-1)">退回</button>
        <button class="btn btn-sm btn-success" onclick="Change_User_Stat(${full.id},'${full.contact_name}',1)">認証為白金</button>
        <button class="btn btn-sm btn-primary" onclick="Change_User_Stat(${full.id},'${full.contact_name}',2)">認証為鑽石</button>
        
        `;
        return upgrade_btn+label ;
        // return data;
    }
</script>

    @endpush
