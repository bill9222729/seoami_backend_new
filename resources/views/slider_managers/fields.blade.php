<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>



<!-- Path Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('path', 'Path:') !!}
    {{-- {!! Form::textarea('path', null, ['class' => 'form-control']) !!} --}}
    <input name="path" type="file" id="path" onchange="getPhotoSize()">


    @if(isset($sliderManager))
        <img style="width:20%;" src="{{$sliderManager->path}}" />
    @endif


</div>

<!-- Click Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('click_type', 'Click Type:') !!}
    {!! Form::select('click_type', ["外連"]) !!}
</div>

<!-- Data Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::textarea('data', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Exp Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('exp_date', 'Exp Date:') !!}
    {!! Form::text('exp_date', null, ['class' => 'form-control','id'=>'exp_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#exp_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sliderManagers.index') }}" class="btn btn-secondary">Cancel</a>
</div>

@push('scripts')
<script type="text/javascript">
    function getPhotoSize(){
        var size = $("#path")[0].files[0].size;
        if(size>=2000000){
            alert("照片最大尺寸為2MB，請重新上傳!");
            $("#path").val(null);
            return false;
        }
    }
</script>
@endpush