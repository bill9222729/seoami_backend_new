<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $sliderManager->title }}</p>
</div>

<!-- Path Field -->
<div class="form-group">
    {!! Form::label('path', 'Path:') !!}
    <img style="width:20%;" src="{{$sliderManager->path}}" />
</div>

<!-- Click Type Field -->
<div class="form-group">
    {!! Form::label('click_type', 'Click Type:') !!}
    <p>{{ $sliderManager->click_type }}</p>
</div>

<!-- Data Field -->
<div class="form-group">
    {!! Form::label('data', 'Data:') !!}
    <p>{{ $sliderManager->data }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $sliderManager->start_date }}</p>
</div>

<!-- Exp Date Field -->
<div class="form-group">
    {!! Form::label('exp_date', 'Exp Date:') !!}
    <p>{{ $sliderManager->exp_date }}</p>
</div>

