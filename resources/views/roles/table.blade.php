<div class="table-responsive-sm">
    <table class="table table-bordered" id="roles-table">
        <thead>
        <th>{{ __('messages.name') }}</th>
        <th>{{ __('messages.permissions') }}</th>
        <th>{{ __('messages.action') }}</th>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
