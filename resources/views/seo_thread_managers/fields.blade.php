<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Free Thread Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('free_thread', 'Free Thread:') !!}
    {!! Form::textarea('free_thread', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Thread Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('payment_thread', 'Payment Thread:') !!}
    {!! Form::textarea('payment_thread', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('seoThreadManagers.index') }}" class="btn btn-secondary">Cancel</a>
</div>
