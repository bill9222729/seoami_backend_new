<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $seoThreadManager->user_id }}</p>
</div>

<!-- Free Thread Field -->
<div class="form-group">
    {!! Form::label('free_thread', 'Free Thread:') !!}
    <p>{{ $seoThreadManager->free_thread }}</p>
</div>

<!-- Payment Thread Field -->
<div class="form-group">
    {!! Form::label('payment_thread', 'Payment Thread:') !!}
    <p>{{ $seoThreadManager->payment_thread }}</p>
</div>

