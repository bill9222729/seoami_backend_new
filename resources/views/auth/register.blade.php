@extends('layouts.auth_layout')
@section('title')
    {{ __('messages.register') }}
@endsection
@section('meta_content')
    - {{ __('messages.register') }} {{ __('messages.to') }} {{getAppName()}}
@endsection
@section('page_css')
    <link rel="stylesheet" href="{{ mix('assets/css/simple-line-icons.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="p-4 account-container w-100">
                <div class="card p-sm-4 p-2 login-group border-0">
                    @if($errors->any())
                        <div class="alert alert-danger text-center mt-2">{{$errors->first()}}</div>
                    @endif
                    <div class="card-body p-sm-4 p-3">
                        <form method="post" action="{{ url('/register') }}" id="registerForm">
                            {{ csrf_field() }}
                            <h1 class="login-group__title mb-2">{{ __('messages.register') }}</h1>
                            <p class="text-muted login-group__sub-title mb-3">{{ __('messages.create_your_account') }}</p>
                            <div class="input-group mb-4">
                                <input type="text"
                                       class="form-control login-group__input {{ $errors->has('name')?'is-invalid':'' }}"
                                       name="name" value="{{ old('name') }}"
                                       placeholder="{{ __('messages.full_name') }}" id="name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <input type="email"
                                       class="form-control login-group__input {{ $errors->has('email')?'is-invalid':'' }}"
                                       name="email" value="{{ old('email') }}" placeholder="{{ __('messages.email') }}"
                                       id="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <input type="password"
                                       class="form-control login-group__input {{ $errors->has('password')?'is-invalid':''}}"
                                       name="password" placeholder="{{ __('messages.password') }}" id="password"
                                       onkeypress="return avoidSpace(event)">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <input type="password" name="password_confirmation"
                                       class="form-control login-group__input"
                                       placeholder="{{ __('messages.confirm_password') }}" id="password_confirmation"
                                       onkeypress="return avoidSpace(event)">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                               </span>
                                @endif
                            </div>
                            <button type="button" id="registerBtn"
                                    class="btn btn-primary btn-block btn-flat mb-4 login-group__register-btn">{{ __('messages.register') }}</button>
                            <a href="{{ url('/login') }}"
                               class="text-center login-group__hover text-decoration-none">{{ __('messages.already_have_membership') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

