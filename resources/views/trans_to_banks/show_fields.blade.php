<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $transToBank->user_id }}</p>
</div>

<!-- Bank No Field -->
<div class="form-group">
    {!! Form::label('bank_no', 'Bank No:') !!}
    <p>{{ $transToBank->bank_no }}</p>
</div>

<!-- Bank Name Field -->
<div class="form-group">
    {!! Form::label('bank_name', 'Bank Name:') !!}
    <p>{{ $transToBank->bank_name }}</p>
</div>

<!-- Sub Bank Name Field -->
<div class="form-group">
    {!! Form::label('sub_bank_name', 'Sub Bank Name:') !!}
    <p>{{ $transToBank->sub_bank_name }}</p>
</div>

<!-- Account Name Field -->
<div class="form-group">
    {!! Form::label('account_name', 'Account Name:') !!}
    <p>{{ $transToBank->account_name }}</p>
</div>

<!-- Account No Field -->
<div class="form-group">
    {!! Form::label('account_no', 'Account No:') !!}
    <p>{{ $transToBank->account_no }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $transToBank->address }}</p>
</div>

<!-- Contact Name Field -->
<div class="form-group">
    {!! Form::label('contact_name', 'Contact Name:') !!}
    <p>{{ $transToBank->contact_name }}</p>
</div>

<!-- Id No Field -->
<div class="form-group">
    {!! Form::label('id_no', 'Id No:') !!}
    <p>{{ $transToBank->id_no }}</p>
</div>

<!-- Paper Photo Field -->
<div class="form-group">
    {!! Form::label('paper_photo', 'Paper Photo:') !!}
    <p>{{ $transToBank->paper_photo }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $transToBank->status }}</p>
</div>

