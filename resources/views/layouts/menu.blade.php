@can('manage_conversations')
<li class="nav-item {{ Request::is('conversations*') ? 'active' : '' }}">
    <a class="nav-link {{ Request::is('conversations*') ? 'active' : '' }}" href="{{ url('conversations')  }}">
        <i class="fa fa-commenting nav-icon mr-4"></i>
        <span>{{ __('messages.conversations') }}</span>
    </a>
</li>
@endcan
@can('manage_users')
    <li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('users*') ? 'active' : '' }}" href="{{ route('users.index') }}">
            <i class="fa fa-users nav-icon mr-4"></i>
            <span>{{ __('messages.users') }}</span>
        </a>
    </li>
@endcan
@can('manage_roles')
    <li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('roles*') ? 'active' : '' }}" href="{{ route('roles.index') }}">
            <i class="fa fa-user nav-icon mr-4"></i>
            <span>{{ __('messages.roles') }}</span>
        </a>
    </li>
@endcan
@can('manage_reported_users')
    <li class="nav-item {{ Request::is('reported-users*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('reported-users*') ? 'active' : '' }}"
           href="{{ route('reported-users.index') }}">
            <i class="fa fa-flag nav-icon mr-4"></i>
            <span>{{ __('messages.reported_user') }}</span>
        </a>
    </li>
@endcan
@if(!Auth::user()->hasRole('Member') && (Auth::user()->hasRole('Admin') || Auth::user()->hasPermissionTo('manage_meetings')))
    <li class="nav-item {{ Request::is('meetings*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('meetings*') ? 'active' : '' }}" href="{{ route('meetings.index') }}">
            <i class="fa fa-stack-exchange nav-icon mr-4"></i>
            <span>{{ __('messages.meetings') }}</span>
        </a>
    </li>
@endif
@role('Member')
    <li class="nav-item {{ Request::is('meetings*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('meetings*') ? 'active' : '' }}" href="{{ route('meetings.member_index') }}">
            <i class="fa fa-stack-exchange nav-icon mr-4"></i>
            <span>{{ __('messages.meetings') }}</span>
        </a>
    </li>
@endrole
@can('manage_settings')
    <li class="nav-item {{ Request::is('settings*') ? 'active' : '' }}">
        <a class="nav-link {{ Request::is('settings*') ? 'active' : '' }}" href="{{ route('settings.index') }}">
            <i class="fa fa-gear nav-icon mr-4"></i>
            <span>{{ __('messages.settings') }}</span>
        </a>
    </li>
@endcan
<li class="nav-item {{ Request::is('sliderManagers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('sliderManagers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>廣告管理</span>
    </a>
</li>
<li class="nav-item {{ Request::is('userStories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('userStories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>用戶動態</span>
    </a>
</li>
<li class="nav-item {{ Request::is('upgradeInfos*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('upgradeInfos.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>升級申請</span>
    </a>
</li>
<li class="nav-item {{ Request::is('walletLogs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('walletLogs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>錢包紀錄</span>
    </a>
</li>
<li class="nav-item {{ Request::is('wallets*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('wallets.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>用戶錢包</span>
    </a>
</li>
<li class="nav-item {{ Request::is('transToBanks*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('transToBanks.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>轉出申請</span>
    </a>
</li>
<li class="nav-item {{ Request::is('seoThreadManagers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('seoThreadManagers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>SEO線程</span>
    </a>
</li>
