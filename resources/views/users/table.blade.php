<div class="table-responsive-sm">
    <table class="table table-bordered" id="users_table">
        <thead>
        <th>{{ __('messages.user') }}</th>
        <th>{{ __('messages.role') }}</th>
        <th>{{ __('messages.group.privacy') }}</th>
        <th>{{ __('messages.email_verified') }}</th>
        <th>{{ __('messages.is_active') }}</th>
        <th>{{ __('messages.impersonate') }}</th>
        <th>{{ __('messages.lv') }}</th>
        <th>{{ __('messages.action') }}</th>
        </thead>
        <tbody></tbody>
    </table>
</div>
