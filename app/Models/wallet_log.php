<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;

/**
 * Class wallet_log
 * @package App\Models
 * @version November 15, 2021, 1:46 am UTC
 *
 * @property integer $user_id
 * @property integer $from_id
 * @property string $reason
 * @property number $point
 * @property number $before_point
 * @property number $after_point
 */
class wallet_log extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'wallet_log';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'from_id',
        'reason',
        'point',
        'before_point',
        'after_point'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'from_id' => 'integer',
        'reason' => 'string',
        'point' => 'float',
        'before_point' => 'float',
        'after_point' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required|integer',
        'from_id' => 'required|integer',
        'reason' => 'required|string|max:191',
        'point' => 'required|numeric',
        'before_point' => 'required|numeric',
        'after_point' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    

    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
