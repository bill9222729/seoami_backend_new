<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;

/**
 * Class user_story
 * @package App\Models
 * @version November 2, 2021, 9:30 am UTC
 *
 * @property integer $user_id
 * @property string $content_type
 * @property string $data
 * @property string $background
 */
class user_story extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'user_story';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'content_type',
        'data',
        'background',
        'desc',
        'views'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'content_type' => 'string',
        'data' => 'string',
        'background' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required|integer',
        'content_type' => 'required|string|max:191',
        'data' => 'required|string',
        'background' => 'required|string|max:191',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    

    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
