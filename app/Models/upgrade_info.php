<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;

/**
 * Class upgrade_info
 * @package App\Models
 * @version November 14, 2021, 12:34 pm UTC
 *
 * @property string $company_name
 * @property string $address
 * @property string $contact_name
 * @property string $email
 * @property string $phone
 * @property string $ref_id
 * @property string $place_id
 */
class upgrade_info extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'upgrade_info';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'company_name',
        'address',
        'contact_name',
        'email',
        'phone',
        'ref_id',
        'place_id',
        'status',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_name' => 'string',
        'address' => 'string',
        'contact_name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'ref_id' => 'string',
        'place_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'company_name' => 'required|string|max:191',
        // 'address' => 'nullable|string|max:191',
        // 'contact_name' => 'required|string|max:191',
        // 'email' => 'required|string|max:191',
        // 'phone' => 'required|string|max:191',
        // 'ref_id' => 'required|string|max:191',
        // 'place_id' => 'nullable|string|max:191',
        // 'created_at' => 'nullable',
        // 'updated_at' => 'nullable',
        // 'deleted_at' => 'nullable'
    ];

    

    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
