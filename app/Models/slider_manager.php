<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;
/**
 * Class slider_manager
 * @package App\Models
 * @version November 2, 2021, 8:29 am UTC
 *
 * @property string $title
 * @property string $path
 * @property string $click_type
 * @property string $data
 * @property string|\Carbon\Carbon $start_date
 * @property string|\Carbon\Carbon $exp_date
 */
class slider_manager extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slider_manager';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'path',
        'click_type',
        'data',
        'start_date',
        'exp_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'path' => 'string',
        'click_type' => 'string',
        'data' => 'string',
        'start_date' => 'datetime',
        'exp_date' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:191',
        // 'path' => 'required|string',
        'click_type' => 'required|string|max:191',
        // 'data' => 'required|string',
        'start_date' => 'required',
        'exp_date' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
