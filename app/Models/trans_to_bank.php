<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;

/**
 * Class trans_to_bank
 * @package App\Models
 * @version November 15, 2021, 3:01 am UTC
 *
 * @property integer $user_id
 * @property string $bank_no
 * @property string $bank_name
 * @property string $sub_bank_name
 * @property string $account_name
 * @property string $account_no
 * @property string $address
 * @property string $contact_name
 * @property string $id_no
 * @property string $paper_photo
 * @property integer $status
 */
class trans_to_bank extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'trans_to_bank';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'bank_no',
        'bank_name',
        'sub_bank_name',
        'account_name',
        'account_no',
        'address',
        'contact_name',
        'id_no',
        'paper_photo',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'bank_no' => 'string',
        'bank_name' => 'string',
        'sub_bank_name' => 'string',
        'account_name' => 'string',
        'account_no' => 'string',
        'address' => 'string',
        'contact_name' => 'string',
        'id_no' => 'string',
        'paper_photo' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'user_id' => 'required|integer',
        // 'bank_no' => 'nullable|string|max:191',
        // 'bank_name' => 'required|string|max:191',
        // 'sub_bank_name' => 'required|string|max:191',
        // 'account_name' => 'required|string|max:191',
        // 'account_no' => 'required|string|max:191',
        // 'address' => 'required|string|max:191',
        // 'contact_name' => 'required|string|max:191',
        // 'id_no' => 'required|string|max:191',
        // 'paper_photo' => 'required|string',
        // 'status' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    

    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
