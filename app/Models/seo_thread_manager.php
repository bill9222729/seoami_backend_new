<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use DateTimeInterface;

/**
 * Class seo_thread_manager
 * @package App\Models
 * @version November 15, 2021, 6:06 am UTC
 *
 * @property integer $user_id
 * @property string $free_thread
 * @property string $payment_thread
 */
class seo_thread_manager extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'seo_thread_manager';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'free_thread',
        'payment_thread'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'free_thread' => 'string',
        'payment_thread' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required|integer',
        'free_thread' => 'nullable|string',
        'payment_thread' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    

    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
