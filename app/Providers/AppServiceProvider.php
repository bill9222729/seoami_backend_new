<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;

use App\Models\upgrade_info;


use App\Models\User;
use App\Observers\Upgrade_InfoObserver;
use App\Observers\UserObserver;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        upgrade_info::observe(Upgrade_InfoObserver::class);
        User::observe(UserObserver::class);
//        \Illuminate\Pagination\Paginator::useBootstrap();

    }
}
