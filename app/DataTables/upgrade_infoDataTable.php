<?php

namespace App\DataTables;

use App\Models\upgrade_info;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use Yajra\DataTables\Html\Column;
class upgrade_infoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'upgrade_infos.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\upgrade_info $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(upgrade_info $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'company_name' => new Column(['title' => '公司名稱', 'data' => 'company_name']),
            'address' => new Column(['title' => '地址', 'data' => 'address']),
            'contact_name' => new Column(['title' => '聯絡人', 'data' => 'contact_name']),
            'email' => new Column(['title' => 'email', 'data' => 'email']),
            'phone' => new Column(['title' => '電話', 'data' => 'phone']),
            'ref_id' => new Column(['title' => '推荐人id', 'data' => 'ref_id']),
            'place_id' => new Column(['title' => '放置人id', 'data' => 'place_id']),
            'status' => new Column(['title' => '審核狀態', 'data' => 'status','render'=>'status_render(data,type,full,meta)']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'upgrade_infos_datatable_' . time();
    }
}
