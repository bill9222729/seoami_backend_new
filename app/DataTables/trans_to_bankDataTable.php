<?php

namespace App\DataTables;

use App\Models\trans_to_bank;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use Yajra\DataTables\Html\Column;
class trans_to_bankDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'trans_to_banks.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\trans_to_bank $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(trans_to_bank $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user_id' => new Column(['title' => 'user_id', 'data' => 'user_id']),
            'bank_no' => new Column(['title' => 'bank_no', 'data' => 'bank_no']),
            'bank_name' => new Column(['title' => 'bank_name', 'data' => 'bank_name']),
            'sub_bank_name' => new Column(['title' => 'sub_bank_name', 'data' => 'sub_bank_name']),
            'account_name' => new Column(['title' => 'account_name', 'data' => 'account_name']),
            'account_no' => new Column(['title' => 'account_no', 'data' => 'account_no']),
            'address' => new Column(['title' => 'address', 'data' => 'address']),
            'contact_name' => new Column(['title' => 'contact_name', 'data' => 'contact_name']),
            'id_no' => new Column(['title' => 'id_no', 'data' => 'id_no']),
            'paper_photo' => new Column(['title' => 'paper_photo', 'data' => 'paper_photo']),
            'status' => new Column(['title' => 'status', 'data' => 'status'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'trans_to_banks_datatable_' . time();
    }
}
