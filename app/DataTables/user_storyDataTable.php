<?php

namespace App\DataTables;

use App\Models\user_story;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use Yajra\DataTables\Html\Column;
class user_storyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'user_stories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\user_story $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(user_story $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user_id' => new Column(['title' => 'user_id', 'data' => 'user_id']),
            'content_type' => new Column(['title' => 'content_type', 'data' => 'content_type']),
            'data' => new Column(['title' => 'data', 'data' => 'data']),
            'desc' => new Column(['title' => 'desc', 'data' => 'desc']),
            
            'background' => new Column(['title' => 'background', 'data' => 'background'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_stories_datatable_' . time();
    }
}
