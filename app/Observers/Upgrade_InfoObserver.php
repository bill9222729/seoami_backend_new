<?php

namespace App\Observers;

use App\Models\upgrade_info;
use App\Models\User;
use App\Models\seo_thread_manager;
use App\Http\Controllers\API\seo_thread_managerAPIController;

class Upgrade_InfoObserver
{
    /**
     * Handle the upgrade_info "created" event.
     *
     * @param  \App\Models\upgrade_info  $upgrade_info
     * @return void
     */
    public function created(upgrade_info $upgrade_info)
    {
        //
    }

    /**
     * Handle the upgrade_info "updated" event.
     *
     * @param  \App\Models\upgrade_info  $upgrade_info
     * @return void
     */
    public function updated(upgrade_info $upgrade_info)
    {

        $end_lv_name = "normal";
        switch ($upgrade_info->status) {
            case '1':
                $end_lv_name = "platinum";
                break;  
                
            case '2':
                    $end_lv_name = "diamond";
                break;
            
            default:
                # code...
                break;
        }
        $user_id = $upgrade_info->user_id;
        $user_item = User::find($user_id);
        if($user_item != null){
            $user_item->lv = $end_lv_name;
            $user_item->save();
        }
        //調整線程
        $add_num = 3;
        if($end_lv_name == 'platinum'){
            $add_num = 6;
        }

        if($end_lv_name == 'diamond'){
            $add_num = 9;
        }

        $thread_item = seo_thread_manager::where('user_id',$user_id)->first();
        $all_thread = [];
        $free_thread_num = 0;
        if($thread_item != null){
            $all_thread = json_decode($thread_item->free_thread,1);
        }
        $free_thread_num = count($all_thread);
       
        $thread_af = $add_num - $free_thread_num;
        if($thread_af > 0){
            for ($i=0; $i <$thread_af ; $i++) { 
                $all_thread[] = seo_thread_managerAPIController::Gen_Thread_Config("free");
    
            }
        }else{
            for ($i=0; $i <$thread_af * -1 ; $i++) { 
                unset($all_thread[$i]);
    
            }
        }
        $thread_json = json_encode(array_values($all_thread));
        $thread_item->free_thread = $thread_json;
        $thread_item->save();
        

     
    }

    /**
     * Handle the upgrade_info "deleted" event.
     *
     * @param  \App\Models\upgrade_info  $upgrade_info
     * @return void
     */
    public function deleted(upgrade_info $upgrade_info)
    {
        //
    }

    /**
     * Handle the upgrade_info "restored" event.
     *
     * @param  \App\Models\upgrade_info  $upgrade_info
     * @return void
     */
    public function restored(upgrade_info $upgrade_info)
    {
        //
    }

    /**
     * Handle the upgrade_info "force deleted" event.
     *
     * @param  \App\Models\upgrade_info  $upgrade_info
     * @return void
     */
    public function forceDeleted(upgrade_info $upgrade_info)
    {
        //
    }
}
