<?php

namespace App\Observers;

use App\Models\User;

use App\Models\seo_thread_manager;
use App\Http\Controllers\API\seo_thread_managerAPIController;
class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $all_thread = [];
        $thread_item = new seo_thread_manager();
        for ($i=0; $i <3 ; $i++) { 
            $all_thread[] = seo_thread_managerAPIController::Gen_Thread_Config("free");

        }

        $thread_json = json_encode(array_values($all_thread));
        $thread_item->free_thread = $thread_json;
        $thread_item->save();
        


    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
