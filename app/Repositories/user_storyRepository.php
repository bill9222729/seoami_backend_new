<?php

namespace App\Repositories;

use App\Models\user_story;
use App\Repositories\BaseRepository;

/**
 * Class user_storyRepository
 * @package App\Repositories
 * @version November 2, 2021, 9:30 am UTC
*/

class user_storyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'content_type',
        'data',
        'background'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return user_story::class;
    }
}
