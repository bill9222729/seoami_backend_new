<?php

namespace App\Repositories;

use App\Models\trans_to_bank;
use App\Repositories\BaseRepository;

/**
 * Class trans_to_bankRepository
 * @package App\Repositories
 * @version November 15, 2021, 3:01 am UTC
*/

class trans_to_bankRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'bank_no',
        'bank_name',
        'sub_bank_name',
        'account_name',
        'account_no',
        'address',
        'contact_name',
        'id_no',
        'paper_photo',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return trans_to_bank::class;
    }
}
