<?php

namespace App\Repositories;

use App\Models\slider_manager;
use App\Repositories\BaseRepository;

/**
 * Class slider_managerRepository
 * @package App\Repositories
 * @version November 2, 2021, 8:29 am UTC
*/

class slider_managerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'path',
        'click_type',
        'data',
        'start_date',
        'exp_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return slider_manager::class;
    }
}
