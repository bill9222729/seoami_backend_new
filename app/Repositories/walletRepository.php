<?php

namespace App\Repositories;

use App\Models\wallet;
use App\Repositories\BaseRepository;

/**
 * Class walletRepository
 * @package App\Repositories
 * @version November 15, 2021, 1:46 am UTC
*/

class walletRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'point'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return wallet::class;
    }
}
