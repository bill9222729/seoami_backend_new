<?php

namespace App\Repositories;

use App\Models\wallet_log;
use App\Repositories\BaseRepository;

/**
 * Class wallet_logRepository
 * @package App\Repositories
 * @version November 15, 2021, 1:46 am UTC
*/

class wallet_logRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'from_id',
        'reason',
        'point',
        'before_point',
        'after_point'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return wallet_log::class;
    }
}
