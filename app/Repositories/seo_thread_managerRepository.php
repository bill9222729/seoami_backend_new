<?php

namespace App\Repositories;

use App\Models\seo_thread_manager;
use App\Repositories\BaseRepository;

/**
 * Class seo_thread_managerRepository
 * @package App\Repositories
 * @version November 15, 2021, 6:06 am UTC
*/

class seo_thread_managerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'free_thread',
        'payment_thread'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return seo_thread_manager::class;
    }
}
