<?php

namespace App\Repositories;

use App\Models\upgrade_info;
use App\Repositories\BaseRepository;

/**
 * Class upgrade_infoRepository
 * @package App\Repositories
 * @version November 14, 2021, 12:34 pm UTC
*/

class upgrade_infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'company_name',
        'address',
        'contact_name',
        'email',
        'phone',
        'ref_id',
        'place_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return upgrade_info::class;
    }
}
