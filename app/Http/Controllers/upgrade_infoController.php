<?php

namespace App\Http\Controllers;

use App\DataTables\upgrade_infoDataTable;
use App\Http\Requests;
use App\Http\Requests\Createupgrade_infoRequest;
use App\Http\Requests\Updateupgrade_infoRequest;
use App\Repositories\upgrade_infoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class upgrade_infoController extends AppBaseController
{
    /** @var  upgrade_infoRepository */
    private $upgradeInfoRepository;

    public function __construct(upgrade_infoRepository $upgradeInfoRepo)
    {
        $this->upgradeInfoRepository = $upgradeInfoRepo;
    }

    /**
     * Display a listing of the upgrade_info.
     *
     * @param upgrade_infoDataTable $upgradeInfoDataTable
     * @return Response
     */
    public function index(upgrade_infoDataTable $upgradeInfoDataTable)
    {
        return $upgradeInfoDataTable->render('upgrade_infos.index');
    }

    /**
     * Show the form for creating a new upgrade_info.
     *
     * @return Response
     */
    public function create()
    {
        return view('upgrade_infos.create');
    }

    /**
     * Store a newly created upgrade_info in storage.
     *
     * @param Createupgrade_infoRequest $request
     *
     * @return Response
     */
    public function store(Createupgrade_infoRequest $request)
    {
        $input = $request->all();

        $upgradeInfo = $this->upgradeInfoRepository->create($input);

        Flash::success('Upgrade Info saved successfully.');

        return redirect(route('upgradeInfos.index'));
    }

    /**
     * Display the specified upgrade_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            Flash::error('Upgrade Info not found');

            return redirect(route('upgradeInfos.index'));
        }

        return view('upgrade_infos.show')->with('upgradeInfo', $upgradeInfo);
    }

    /**
     * Show the form for editing the specified upgrade_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            Flash::error('Upgrade Info not found');

            return redirect(route('upgradeInfos.index'));
        }

        return view('upgrade_infos.edit')->with('upgradeInfo', $upgradeInfo);
    }

    /**
     * Update the specified upgrade_info in storage.
     *
     * @param  int              $id
     * @param Updateupgrade_infoRequest $request
     *
     * @return Response
     */
    public function update($id, Updateupgrade_infoRequest $request)
    {
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            Flash::error('Upgrade Info not found');

            return redirect(route('upgradeInfos.index'));
        }

        $upgradeInfo = $this->upgradeInfoRepository->update($request->all(), $id);

        Flash::success('Upgrade Info updated successfully.');

        return redirect(route('upgradeInfos.index'));
    }

    /**
     * Remove the specified upgrade_info from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            Flash::error('Upgrade Info not found');

            return redirect(route('upgradeInfos.index'));
        }

        $this->upgradeInfoRepository->delete($id);

        Flash::success('Upgrade Info deleted successfully.');

        return redirect(route('upgradeInfos.index'));
    }
}
