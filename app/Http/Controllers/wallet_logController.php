<?php

namespace App\Http\Controllers;

use App\DataTables\wallet_logDataTable;
use App\Http\Requests;
use App\Http\Requests\Createwallet_logRequest;
use App\Http\Requests\Updatewallet_logRequest;
use App\Repositories\wallet_logRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class wallet_logController extends AppBaseController
{
    /** @var  wallet_logRepository */
    private $walletLogRepository;

    public function __construct(wallet_logRepository $walletLogRepo)
    {
        $this->walletLogRepository = $walletLogRepo;
    }

    /**
     * Display a listing of the wallet_log.
     *
     * @param wallet_logDataTable $walletLogDataTable
     * @return Response
     */
    public function index(wallet_logDataTable $walletLogDataTable)
    {
        return $walletLogDataTable->render('wallet_logs.index');
    }

    /**
     * Show the form for creating a new wallet_log.
     *
     * @return Response
     */
    public function create()
    {
        return view('wallet_logs.create');
    }

    /**
     * Store a newly created wallet_log in storage.
     *
     * @param Createwallet_logRequest $request
     *
     * @return Response
     */
    public function store(Createwallet_logRequest $request)
    {
        $input = $request->all();

        $walletLog = $this->walletLogRepository->create($input);

        Flash::success('Wallet Log saved successfully.');

        return redirect(route('walletLogs.index'));
    }

    /**
     * Display the specified wallet_log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            Flash::error('Wallet Log not found');

            return redirect(route('walletLogs.index'));
        }

        return view('wallet_logs.show')->with('walletLog', $walletLog);
    }

    /**
     * Show the form for editing the specified wallet_log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            Flash::error('Wallet Log not found');

            return redirect(route('walletLogs.index'));
        }

        return view('wallet_logs.edit')->with('walletLog', $walletLog);
    }

    /**
     * Update the specified wallet_log in storage.
     *
     * @param  int              $id
     * @param Updatewallet_logRequest $request
     *
     * @return Response
     */
    public function update($id, Updatewallet_logRequest $request)
    {
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            Flash::error('Wallet Log not found');

            return redirect(route('walletLogs.index'));
        }

        $walletLog = $this->walletLogRepository->update($request->all(), $id);

        Flash::success('Wallet Log updated successfully.');

        return redirect(route('walletLogs.index'));
    }

    /**
     * Remove the specified wallet_log from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            Flash::error('Wallet Log not found');

            return redirect(route('walletLogs.index'));
        }

        $this->walletLogRepository->delete($id);

        Flash::success('Wallet Log deleted successfully.');

        return redirect(route('walletLogs.index'));
    }
}
