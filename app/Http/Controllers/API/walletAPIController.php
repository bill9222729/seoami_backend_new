<?php

namespace App\Http\Controllers\API;
use Auth;
use App\Http\Requests\API\CreatewalletAPIRequest;
use App\Http\Requests\API\UpdatewalletAPIRequest;
use App\Models\wallet;
use App\Models\wallet_log;
use App\Models\User;
use App\Repositories\walletRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\walletResource;
use Response;
use Log;
/**
 * Class walletController
 * @package App\Http\Controllers\API
 */

class walletAPIController extends AppBaseController
{
    /** @var  walletRepository */
    private $walletRepository;

    public function __construct(walletRepository $walletRepo)
    {
        $this->walletRepository = $walletRepo;
    }
    public function Create_All_User_Wallet(){
        $all_user = User::all();
        foreach ($all_user as $key => $value) {
            $e = wallet::where('user_id', $value->id)->first();
            if($e == null){
                $wa = new wallet();
                $wa->user_id = $value->id;
                $wa->point = 0;
                $wa->save();
            }
        }
    }
    public function my_wallet(){
        $user_id = getLoggedInUserId();
        $wallet = wallet::where('user_id',$user_id)->first();
        return $this->sendResponse($wallet, 'Wallets retrieved successfully');


    }

    public function Trans(Request $request){
        $from_id = 0;
        if($request->has('from_id')){
            $from_id =$request->from_id;
        }else{
           $from_id =  getLoggedInUserId();
        }
       $rs = self::Trans_fn($from_id,$request->recv_id,$request->amount,$request->reason);
       return $rs;
    }
    public static function  Trans_fn($from_id,$recv_id,$amount,$reason){
            
            $end = ['status'=>false,'reason'=>''];
            if($amount < 0){
                $end['reason'] = '金額錯誤';
                return json_encode($end);
            }

            $from_wallet = wallet::where('user_id', $from_id)->first();
            $recv_wallet = wallet::where('user_id', $recv_id)->first();
            if($from_wallet ==null || $recv_wallet ==null){
                $end['reason'] = '用戶不存在';
                return json_encode($end);
            }

          
            if($from_wallet->point < $amount)
            {
                $end['reason'] = '餘額不足';
                return json_encode($end);
            }
            
            $from_wallet->point -= $amount;
            $from_wallet->save();
         
            $recv_wallet->point += $amount;
            $recv_wallet->save();

            self::Trans_Log($from_id,$recv_id,$amount,$reason);
            $end['status'] = true;
            $end['reason'] = "轉帳成功";
         
            return json_encode($end);
                    
        }

        public static function Trans_Log($from_id,$recv_id,$amount,$reason){
            
            // $from_user = User::find($from_id);
            $from_wallet = wallet::where('user_id', $from_id)->first();
            // $recv_user = User::find($recv_id);
            $recv_wallet = wallet::where('user_id', $recv_id)->first();
            
            #發出者
            $log = new wallet_log();
            $log->user_id = $recv_id;
            $log->from_id = $from_id;
            $log->reason = $reason;
            $log->point = $amount*-1;
            $log->before_point = $from_wallet->point;
            $log->after_point = $from_wallet->point + ($amount*-1);
            $log->save();


            #接收者
            $recv_log = new wallet_log();
            $recv_log->user_id = $recv_id;
            $recv_log->from_id = $from_id;
            $recv_log->reason = $reason;
            $recv_log->point = $amount;
            $recv_log->before_point = $from_wallet->point;
            $recv_log->after_point = $from_wallet->point + ($amount);
            $recv_log->save();
            
                        
        }



    /**
     * Display a listing of the wallet.
     * GET|HEAD /wallets
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $wallets = $this->walletRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(walletResource::collection($wallets), 'Wallets retrieved successfully');
    }

    /**
     * Store a newly created wallet in storage.
     * POST /wallets
     *
     * @param CreatewalletAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatewalletAPIRequest $request)
    {
        $input = $request->all();

        $wallet = $this->walletRepository->create($input);

        return $this->sendResponse(new walletResource($wallet), 'Wallet saved successfully');
    }

    /**
     * Display the specified wallet.
     * GET|HEAD /wallets/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var wallet $wallet */
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        return $this->sendResponse(new walletResource($wallet), 'Wallet retrieved successfully');
    }

    /**
     * Update the specified wallet in storage.
     * PUT/PATCH /wallets/{id}
     *
     * @param int $id
     * @param UpdatewalletAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatewalletAPIRequest $request)
    {
        $input = $request->all();

        /** @var wallet $wallet */
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        $wallet = $this->walletRepository->update($input, $id);

        return $this->sendResponse(new walletResource($wallet), 'wallet updated successfully');
    }

    /**
     * Remove the specified wallet from storage.
     * DELETE /wallets/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var wallet $wallet */
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        $wallet->delete();

        return $this->sendSuccess('Wallet deleted successfully');
    }
}
