<?php

namespace App\Http\Controllers\API;

use App\Events\UserEvent;
use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\SendMessageRequest;
use App\Models\ChatRequestModel;

use App\Models\Conversation;
use App\Models\User;
use App\Repositories\ChatRepository;
use Auth;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Storage;
use Log;
use Illuminate\Notifications\Notifiable;

use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

// Create a notification for user and add the following code
class AccountActivated extends Notification
{
    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $rs= FcmMessage::create()
            ->setData(['data1' => 'value', 'data2' => 'value2'])
            ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
                ->setTitle('Account Activated')
                ->setBody('Your account has been activated.'))
                // ->setImage('http://example.com/url-to-image-here.png'))
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setNotification(AndroidNotification::create()->setColor('#0A0A0A'))
            )->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios')));
                    // dd($rs);
                    return $rs;
    }
}
/**
 * Class ChatAPIController
 */
class ChatAPIController extends AppBaseController
{
    /** @var ChatRepository */
    private $chatRepository;
    use Notifiable;
    /**
     * Create a new controller instance.
     *
     * @param  ChatRepository  $chatRepository
     */
    public function __construct(ChatRepository $chatRepository)
    {
        $this->chatRepository = $chatRepository;
    }
    public function getChatRequest(Request $request){
        // dd(getLoggedInUserId());
        
        $chatRequest = ChatRequestModel::where('owner_id',getLoggedInUserId())->where('status',0)->get()->toArray();
        foreach ($chatRequest as $key => $value) {
         
           $user_data = User::where('id',intval($value['from_id']))->first()->toArray();
           $chatRequest[$key]['user'] = $user_data;

        }
        // dd($chatRequest);
        return $this->sendResponse($chatRequest, 'chatRequest retrieved successfully.');
    }
    /**
     * This function return latest conversations of users.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getLatestConversations(Request $request)
    {
        $input = $request->all();
        $conversations = $this->chatRepository->getLatestConversations($input);
            //去掉未通過的對話請求
      
        // $end = [];
        // foreach ($conversations as $key => $value) {
        //     if($value['status'] == 1){
        //         $end[] = $value;
        //     }
        // }
        return $this->sendResponse(['conversations' => $conversations], 'Conversations retrieved successfully.');
    }

    /**
     * This function return latest conversations of users.
     *
     * @return JsonResponse
     */
    public function getArchiveConversations(Request $request)
    {
        $input = $request->all();
        $input['isArchived'] = 1;
        $conversations = $this->chatRepository->getLatestConversations($input);

        return $this->sendResponse(['conversations' => $conversations], 'Conversations retrieved successfully.');
    }

    /**
     * @param SendMessageRequest $request
     *
     * @return JsonResponse
     */
    public function sendMessage(SendMessageRequest $request)
    {
        $conversation = $this->chatRepository->sendMessage($request->all());
        try {
            $from_user_data = User::find(getLoggedInUserId());
            $this->Send_User_Notification($request->to_id,"新的訊息！",$from_user_data->name.":".$request->message);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->sendResponse(['message' => $conversation], 'Message sent successfully.');

        
    }
    function Send_User_Notification($user_id,$title,$message){
        $user_data = User::find($user_id);
        $tokens = $user_data->device_token;
        // dd($tokens);
        if($tokens == ""){ return;}
        $tokens = json_decode($tokens,1);
        $this->sendNotification($title, $message,$tokens);
    }
    function sendNotification($title,$message,$tokens)
{
    
    // $usernames = $request->all()['friend_usernames'];
    // $dialog_id = $request->all()['dialog_id'];

    // foreach ($usernames as $username) {
    //     $friendToken[] = DB::table('users')->where('user_name', $username)
    //         ->get()->pluck('device_token')[0];
    // }

    $url = 'https://fcm.googleapis.com/fcm/send';

        $content = array
        (
            'title'	=> $title,
            'body' 	=> $message        );
        $fields = array
	(
        
        // 'to'		    =>'t'
		'notification'	=> $content,
        'registration_ids'=> $tokens,
	);
      
        $headers = array(
            'Authorization: key=AAAAm_ai8xI:APA91bGWM9aEnJ0XtTthDRamjgtdhkAhEO-YBx6-VUXWGELwzCwQjfdHe06PTIaalX6nDjf6RArGsFiRLkUxfQOWEW7hUft1LrnlI6b2PKTCoRLPvdu3UuqyU20AlqLPGVytl4ynv_mS',
            'Content-type: Application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        
  

    // dd($result);
    return $result;
}
public function pusher(){
    // $this->sendNotification('標','內','1');
}

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function updateConversationStatus(Request $request)
    {
        $data = $this->chatRepository->markMessagesAsRead($request->all());

        return $this->sendResponse($data, 'Status updated successfully.');
    }

    /**
     * @param  Request  $request
     *
     * @throws ApiOperationFailedException
     *
     * @return JsonResponse
     */
    public function addAttachment(Request $request)
    {
        $files = $request->file('file');
        foreach ($files as $file) {
            $fileData['attachment'] = $this->chatRepository->addAttachment($file);
            $extension = $file->getClientOriginalExtension();
            $fileData['message_type'] = $this->chatRepository->getMessageTypeByExtension($extension);
            $fileData['file_name'] = $file->getClientOriginalName();
            $fileData['unique_code'] = uniqid();
            $data['data'][] = $fileData;
        }
        $data['success'] = true;

        return $this->sendData($data);
    }

    /**
     * @param  int|string  $id
     *
     * @return JsonResponse
     */
    public function deleteConversation($id)
    {
        if (is_string($id) && ! is_numeric($id)) {
            $this->chatRepository->deleteGroupConversation($id);
        } else {
            $this->chatRepository->deleteConversation($id);
        }

        return $this->sendSuccess('Conversation deleted successfully.');
    }

    /**
     * @param  Conversation  $conversation
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function deleteMessage(Conversation $conversation, Request $request)
    {
        $deleteMessageTime = config('configurable.delete_message_time');
        if ($conversation->time_from_now_in_min > $deleteMessageTime) {
            return $this->sendError('You can not delete message older than '.$deleteMessageTime.' minutes.', 422);
        }

        if ($conversation->from_id != getLoggedInUserId()) {
            return $this->sendError('You can not delete this message.', 403);
        }

        $previousMessageId = $request->get('previousMessageId');
        $previousMessage = $this->chatRepository->find($previousMessageId);
        $this->chatRepository->deleteMessage($conversation->id);

        return $this->sendResponse(['previousMessage' => $previousMessage], 'Message deleted successfully.');
    }

    /**
     * @param  Conversation  $conversation
     *
     * @return JsonResponse
     */
    public function show(Conversation $conversation)
    {
        return $this->sendResponse($conversation->toArray(), 'Conversation retrieved successfully');
    }

    /**
     * @param  Conversation  $conversation
     * @param  Request  $request
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function deleteMessageForEveryone(Conversation $conversation, Request $request)
    {
        $deleteMessageTime = config('configurable.delete_message_for_everyone_time');
        if ($conversation->time_from_now_in_min > $deleteMessageTime) {
            return $this->sendError('You can not delete message older than '.$deleteMessageTime.' minutes.', 422);
        }

        if ($conversation->from_id != getLoggedInUserId()) {
            return $this->sendError('You can not delete this message.', 403);
        }

        $conversation->delete();

        $previousMessageId = $request->get('previousMessageId');
        $previousMessage = $this->chatRepository->find($previousMessageId);
        unset($previousMessage->replayMessage);

        broadcast(new UserEvent(
            [
                'id'              => $conversation->id,
                'type'            => User::MESSAGE_DELETED,
                'from_id'         => $conversation->from_id,
                'previousMessage' => $previousMessage,
            ], $conversation->to_id))->toOthers();

        return $this->sendResponse(['previousMessage' => $previousMessage], 'Message deleted successfully.');
    }

    /**
     * @param SendMessageRequest $request
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function sendChatRequest(SendMessageRequest $request)
    {
        $isRequestSend = $this->chatRepository->sendChatRequest($request->all());
        
        if ($isRequestSend) {
            try {
                $to_id = $request->to_id;
                $from_user_data = User::find(getLoggedInUserId());
                $this->Send_User_Notification($to_id,"談合作邀請",$from_user_data->name."邀請與您洽談合作");
            } catch (\Throwable $th) {
                //throw $th;
            }
            return $this->sendSuccess('Chat request send successfully.');
        }

        return $this->sendError('Chat request has already been sent.');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function acceptChatRequest(Request $request)
    {
        $chatRequestModel = ChatRequestModel::whereId($request->id)->first();
        // Log::debug($request->all());
        $chatRequestModel->status = ChatRequestModel::STATUS_ACCEPTED;
        $chatRequestModel->save();

        $input = $chatRequestModel->toArray();
        $input['message'] = $chatRequestModel->receiver->name.' has accepted your chat request.';
        $this->chatRepository->sendAcceptDeclineChatRequestNotification($input, User::CHAT_REQUEST_ACCEPTED);

        return $this->sendResponse($chatRequestModel, 'Chat request accepted successfully.');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function declineChatRequest(Request $request)
    {
        $chatRequestModel = ChatRequestModel::find($request->id);
        $chatRequestModel->status = ChatRequestModel::STATUS_DECLINE;
        $chatRequestModel->save();

        Conversation::whereFromId($chatRequestModel->from_id)->whereToId($chatRequestModel->owner_id)->update(['status' => 1]);

        $input = $chatRequestModel->toArray();
        $input['message'] = $chatRequestModel->receiver->name.' has declined your chat request.';
        $this->chatRepository->sendAcceptDeclineChatRequestNotification($input);

        return $this->sendResponse($chatRequestModel, 'You have declined given user request !');
    }

    /**
     * @param Request $request
     *
     * @throws ApiOperationFailedException
     *
     * @return JsonResponse
     */
    public function imageUpload(Request $request)
    {
        $input = $request->all();
        $images = $input['images'];
        unset($input['images']);
        $input['from_id'] = Auth::id();
        $input['to_type'] = Conversation::class;
        $conversation = [];
        foreach ($images as $image) {
            $fileName = Conversation::uploadBase64Image($image, Conversation::PATH);
            $input['message'] = $fileName;
            $input['status'] = 0;
            $input['message_type'] = 1;
            $input['file_name'] = $fileName;
            $conversation[] = $this->chatRepository->sendMessage($input);
        }

        return $this->sendResponse($conversation, 'File uploaded');
    }

    public function file_upload($file)
    {   
        $dir_name = "chat";
        $head = "/uploads/";
        $pat = Storage::put($dir_name, $file);
        return $head.$pat;

    }
    /**
     * @param Request $request
     *
     * @throws ApiOperationFailedException
     *
     * @return JsonResponse
     */
    public function imageUpload_APP(Request $request)
    {
        $input = $request->all();
       
        if($request->file('images')!= null){
            // print_r("Hello");
            $save_path  = self::file_upload($request->file('images'));

            $fileName = $save_path;
        }else{
            return 0;
        }

        $input['from_id'] = Auth::id();
        $input['to_type'] = Conversation::class;
        $conversation = [];
        // foreach ($images as $image) {
        //     // $fileName = Conversation::uploadBase64Image($image, Conversation::PATH);
          
        // }
        $input['message'] = $fileName;
        $input['status'] = 0;
        $input['message_type'] = 1;
        $input['file_name'] = $fileName;
        $conversation[] = $this->chatRepository->sendMessage($input);
        return $this->sendResponse($conversation, 'File uploaded');
    }
}
