<?php

namespace App\Http\Controllers\API;

use App;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CreateUserStatusRequest;
use App\Http\Requests\UpdateUserNotificationRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Models\ArchivedUser;
use App\Models\BlockedUser;
use App\Models\Group;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\Conversation;
use App\Repositories\UserDeviceRepository;
use App\Repositories\UserRepository;
use Auth;
use Carbon\Carbon;
use Exception;
use Hash;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Storage;
/**
 * Class UserAPIController
 */
class UserAPIController extends AppBaseController
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return JsonResponse
     */
    public function getUsersList()
    {
        $myContactIds = $this->userRepository->myContactIds();
        $userIds = BlockedUser::orwhere('blocked_by', getLoggedInUserId())
            ->orWhere('blocked_to', getLoggedInUserId())
            ->pluck('blocked_by', 'blocked_to')
            ->toArray();

        $userIds = array_unique(array_merge($userIds, array_keys($userIds)));
        $userIds = array_unique(array_merge($userIds, $myContactIds));

        $users = User::whereNotIn('id', $userIds)
            ->orderBy('name', 'asc')
            ->select(['id', 'is_online', 'gender', 'photo_url', 'name'])
            ->limit(50)
            ->get()
            ->except(getLoggedInUserId());

    
        return $this->sendResponse(['users' => $users], 'Users retrieved successfully.');
    }
    

    
    /**
     * @return JsonResponse
     */
    public function getFriendList()
    {
        $myContactIds = $this->userRepository->myContactIds();
        $userIds = BlockedUser::orwhere('blocked_by', getLoggedInUserId())
            ->orWhere('blocked_to', getLoggedInUserId())
            ->pluck('blocked_by', 'blocked_to')
            ->toArray();

        $userIds = array_unique(array_merge($userIds, array_keys($userIds)));
        $userIds = array_unique(array_merge($userIds, $myContactIds));

        $users = User::whereNotIn('id', $userIds)
            ->orderBy('name', 'asc')
            ->select(['id', 'is_online', 'gender', 'photo_url', 'name'])
            ->limit(50)
            ->get()
            ->except(getLoggedInUserId());

        return $this->sendResponse(['users' => $users], 'Users retrieved successfully.');
    }
    


    /**
     * @return JsonResponse
     */
    public function getUsers()
    {
        $users = User::orderBy('name', 'asc')->get()->except(getLoggedInUserId());

        return $this->sendResponse(['users' => $users], 'Users retrieved successfully.');
    }

    /**
     * @return JsonResponse
     */
    public function getProfile()
    {
        /** @var User $authUser * */
        $authUser = getLoggedInUser();
        $authUser->roles;
        $authUser = $authUser->apiObj();

        return $this->sendResponse(['user' => $authUser], 'Users retrieved successfully.');
    }

    public function Get_Other_Profile($id){
        $data = User::find($id);
        return $this->sendResponse(['user' => $data], 'Users retrieved successfully.');
    }
    public function profile_upload_img(Request $request){
        $user_id = Auth::id();
        $input = $request->all();
        // print_r(gettype($input['data']));
        if($request->file('data')!= null){
            // print_r("Hello");
            $save_path  = self::file_upload($request->file('data'));

            $save_path = str_replace("/uploads/users/","",$save_path);
        }
        $user_item = Auth::user();
        $user_item->photo_url = $save_path;
        $user_item->save();

    }
    public function file_upload($file)
    {   
        $dir_name = "users";
        $head = "/uploads/";
        $pat = Storage::put($dir_name, $file);
        return $head.$pat;

    }

    /**
     * @param  UpdateUserProfileRequest  $request
     *
     * @return JsonResponse
     */
    public function updateProfile(UpdateUserProfileRequest $request)
    {
        try {
            $this->userRepository->updateProfile($request->all());

            return $this->sendSuccess('Profile updated successfully.');
        } catch (Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function updateLastSeen(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $lastSeen = ($request->has('status') && $request->get('status') > 0) ? null : Carbon::now();

        $user->update(['last_seen' => $lastSeen, 'is_online' => $request->get('status')]);

        return $this->sendResponse(['user' => $user], 'Last seen updated successfully.');
    }

    /**
     * @param  int  $id
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getConversation($id, Request $request)
    {
        $input = $request->all();
        $data = $this->userRepository->getConversation($id, $input);

        return $this->sendResponse($data, 'Conversation retrieved successfully.');
    }

    /**
     * @param  ChangePasswordRequest  $request
     *
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $user->update(['password' => $input['password']]);

        return $this->sendSuccess('Password updated successfully.');
    }

    /**
     * @param  UpdateUserNotificationRequest  $request
     *
     * @return JsonResponse
     */
    public function updateNotification(UpdateUserNotificationRequest $request)
    {
        $input = $request->all();
        $input['is_subscribed'] = ($input['is_subscribed'] == 'true') ? true : false;

        $this->userRepository->storeAndUpdateNotification($input);

        return $this->sendSuccess('Notification updated successfully.');
    }

    /**
     * @return JsonResponse
     */
    public function removeProfileImage()
    {
        /** @var User $user */
        $user = Auth::user();

        $user->deleteImage();

        return $this->sendSuccess('Profile image deleted successfully.');
    }

    /**
     * @param $ownerId
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function archiveChat($ownerId)
    {
        $archivedUser = ArchivedUser::whereOwnerId($ownerId)->whereArchivedBy(getLoggedInUserId())->first();
        if (is_string($ownerId) && ! is_numeric($ownerId)) {
            $ownerType = Group::class;
        } else {
            $ownerType = User::class;
        }

        if (empty($archivedUser)) {
            ArchivedUser::create([
                'owner_id'    => $ownerId,
                'owner_type'  => $ownerType,
                'archived_by' => getLoggedInUserId(),
            ]);
        } else {
            $archivedUser->delete();

            return $this->sendResponse(['archived' => false], 'Chat unarchived successfully.');
        }

        return $this->sendResponse(['archived' => true], 'Chat archived successfully.');
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function updatePlayerId(Request $request)
    {
        $playerId = $request->get('player_id');
        $input['user_id'] = Auth::id();
        $input['player_id'] = $playerId;

        /** @var UserDeviceRepository $deviceRepo */
        $deviceRepo = App::make(UserDeviceRepository::class);
        $deviceRepo->store($input);

        $myPlayerIds = UserDevice::whereUserId(Auth::id())->get();

        return $this->sendResponse($myPlayerIds, 'Player updated successfully.');
    }

    /**
     * @param CreateUserStatusRequest $request
     *
     * @return JsonResponse
     */
    public function setUserCustomStatus(CreateUserStatusRequest $request)
    {
        $input = $request->only(['emoji', 'status', 'emoji_short_name']);

        $userStatus = $this->userRepository->setUserCustomStatus($input);

        return $this->sendResponse($userStatus, 'Your status set successfully.');
    }

    /**
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function clearUserCustomStatus()
    {
        $this->userRepository->clearUserCustomStatus();

        return $this->sendSuccess('Your status cleared successfully.');
    }
    public function reg_device_token(Request $request){
        
        
        $self_id = getLoggedInUserId();
        $user_data = User::find($self_id);
        $device_list = $user_data->device_token;
        if($device_list == ""){
            $device_list = [];
        }else{
            $device_list = json_decode($device_list,1);
        }
        
        if(!in_array($request->token,$device_list)){
            $device_list[] = $request->token;

        $user_data->device_token = json_encode($device_list);
        $user_data->save();
        }else{
            
        return $this->sendResponse([],$request->token. 'token exist!');
         
        }
        return $this->sendResponse([],$request->token. 'token save ok!');
        // return "token save ok!";
    }

    public function check_is_friend($id){

        $self_id = getLoggedInUserId();
     
        $rs = $this->userRepository->checkIsMyContact($id);
        if($rs){
            return "1";
        }else{
            return "0";
        }
    }
    /*
    * @return JsonResponse
    */
    public function myContacts()
    {
        $self_id = getLoggedInUserId();
     
        $myContactIds = $this->userRepository->myContactIds();
        // dd($myContactIds);
        $users = User::with(['userStatus'=> function (HasOne $query) {
            $query->select(['status', 'emoji']);
        }])
            ->whereIn('id', $myContactIds)
            ->select(['id', 'name', 'photo_url', 'is_online','lv'])
            ->orderBy('name', 'asc')
            ->limit(100)
            ->get();
    $users = $users->toArray();
    foreach ($users as $key => $value) {
     
        $chat_data = Conversation::where('from_id', $value['id'])->where('to_id',$self_id)->orderBy('created_at','DESC')->first();
      
        $users[$key]['last_msg'] =null;
        if($chat_data != null){
            $users[$key]['last_msg'] = $chat_data;
        }
    }

        return $this->sendResponse([
            'users' => $users,
            'myContactIds' => $myContactIds,
        ], 'Users retrieved successfully.');
    }
}
