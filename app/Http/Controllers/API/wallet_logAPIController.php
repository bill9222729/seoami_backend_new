<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createwallet_logAPIRequest;
use App\Http\Requests\API\Updatewallet_logAPIRequest;
use App\Models\wallet_log;
use App\Repositories\wallet_logRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\wallet_logResource;
use Response;

/**
 * Class wallet_logController
 * @package App\Http\Controllers\API
 */

class wallet_logAPIController extends AppBaseController
{
    /** @var  wallet_logRepository */
    private $walletLogRepository;

    public function __construct(wallet_logRepository $walletLogRepo)
    {
        $this->walletLogRepository = $walletLogRepo;
    }

    /**
     * Display a listing of the wallet_log.
     * GET|HEAD /walletLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $walletLogs = $this->walletLogRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(wallet_logResource::collection($walletLogs), 'Wallet Logs retrieved successfully');
    }

    /**
     * Store a newly created wallet_log in storage.
     * POST /walletLogs
     *
     * @param Createwallet_logAPIRequest $request
     *
     * @return Response
     */
    public function store(Createwallet_logAPIRequest $request)
    {
        $input = $request->all();

        $walletLog = $this->walletLogRepository->create($input);

        return $this->sendResponse(new wallet_logResource($walletLog), 'Wallet Log saved successfully');
    }

    /**
     * Display the specified wallet_log.
     * GET|HEAD /walletLogs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var wallet_log $walletLog */
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            return $this->sendError('Wallet Log not found');
        }

        return $this->sendResponse(new wallet_logResource($walletLog), 'Wallet Log retrieved successfully');
    }

    /**
     * Update the specified wallet_log in storage.
     * PUT/PATCH /walletLogs/{id}
     *
     * @param int $id
     * @param Updatewallet_logAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatewallet_logAPIRequest $request)
    {
        $input = $request->all();

        /** @var wallet_log $walletLog */
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            return $this->sendError('Wallet Log not found');
        }

        $walletLog = $this->walletLogRepository->update($input, $id);

        return $this->sendResponse(new wallet_logResource($walletLog), 'wallet_log updated successfully');
    }

    /**
     * Remove the specified wallet_log from storage.
     * DELETE /walletLogs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var wallet_log $walletLog */
        $walletLog = $this->walletLogRepository->find($id);

        if (empty($walletLog)) {
            return $this->sendError('Wallet Log not found');
        }

        $walletLog->delete();

        return $this->sendSuccess('Wallet Log deleted successfully');
    }
}
