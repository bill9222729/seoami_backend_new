<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createupgrade_infoAPIRequest;
use App\Http\Requests\API\Updateupgrade_infoAPIRequest;
use App\Models\upgrade_info;
use App\Repositories\upgrade_infoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\upgrade_infoResource;
use Response;
use Auth;

/**
 * Class upgrade_infoController
 * @package App\Http\Controllers\API
 */

class upgrade_infoAPIController extends AppBaseController
{
    /** @var  upgrade_infoRepository */
    private $upgradeInfoRepository;

    public function __construct(upgrade_infoRepository $upgradeInfoRepo)
    {
        $this->upgradeInfoRepository = $upgradeInfoRepo;
    }

    /**
     * Display a listing of the upgrade_info.
     * GET|HEAD /upgradeInfos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $upgradeInfos = $this->upgradeInfoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(upgrade_infoResource::collection($upgradeInfos), 'Upgrade Infos retrieved successfully');
    }
    public function create_upgrade(Createupgrade_infoAPIRequest $request){
        $input = $request->all();
        $user_id = Auth::id();
        $exist_submit = upgrade_info::where('user_id', $user_id)->first();
        if($exist_submit != null){
            $id = $exist_submit->id;
            $upgradeInfo = $exist_submit;
            $upgradeInfo->status = 0;
            $upgradeInfo->save();
            
            
        }else{
            $input['user_id'] = $user_id;
            $upgradeInfo = $this->upgradeInfoRepository->create($input);
        }

        

        return $this->sendResponse(new upgrade_infoResource($upgradeInfo), 'Upgrade Info saved successfully');
    }
    /**
     * Store a newly created upgrade_info in storage.
     * POST /upgradeInfos
     *
     * @param Createupgrade_infoAPIRequest $request
     *
     * @return Response
     */
    public function store(Createupgrade_infoAPIRequest $request)
    {
        $input = $request->all();

        $upgradeInfo = $this->upgradeInfoRepository->create($input);

        return $this->sendResponse(new upgrade_infoResource($upgradeInfo), 'Upgrade Info saved successfully');
    }

    /**
     * Display the specified upgrade_info.
     * GET|HEAD /upgradeInfos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var upgrade_info $upgradeInfo */
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            return $this->sendError('Upgrade Info not found');
        }

        return $this->sendResponse(new upgrade_infoResource($upgradeInfo), 'Upgrade Info retrieved successfully');
    }

    /**
     * Update the specified upgrade_info in storage.
     * PUT/PATCH /upgradeInfos/{id}
     *
     * @param int $id
     * @param Updateupgrade_infoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateupgrade_infoAPIRequest $request)
    {
        $input = $request->all();

        /** @var upgrade_info $upgradeInfo */
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            return $this->sendError('Upgrade Info not found');
        }

        $upgradeInfo = $this->upgradeInfoRepository->update($input, $id);

        return $this->sendResponse(new upgrade_infoResource($upgradeInfo), 'upgrade_info updated successfully');
    }

    /**
     * Remove the specified upgrade_info from storage.
     * DELETE /upgradeInfos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var upgrade_info $upgradeInfo */
        $upgradeInfo = $this->upgradeInfoRepository->find($id);

        if (empty($upgradeInfo)) {
            return $this->sendError('Upgrade Info not found');
        }

        $upgradeInfo->delete();

        return $this->sendSuccess('Upgrade Info deleted successfully');
    }
}
