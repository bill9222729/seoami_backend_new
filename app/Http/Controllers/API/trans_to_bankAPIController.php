<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtrans_to_bankAPIRequest;
use App\Http\Requests\API\Updatetrans_to_bankAPIRequest;
use App\Models\trans_to_bank;
use App\Repositories\trans_to_bankRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\trans_to_bankResource;
use Response;
use Auth;
use Storage;

/**
 * Class trans_to_bankController
 * @package App\Http\Controllers\API
 */

class trans_to_bankAPIController extends AppBaseController
{
    /** @var  trans_to_bankRepository */
    private $transToBankRepository;

    public function __construct(trans_to_bankRepository $transToBankRepo)
    {
        $this->transToBankRepository = $transToBankRepo;
    }

    /**
     * Display a listing of the trans_to_bank.
     * GET|HEAD /transToBanks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $transToBanks = $this->transToBankRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(trans_to_bankResource::collection($transToBanks), 'Trans To Banks retrieved successfully');
    }

    /**
     * Store a newly created trans_to_bank in storage.
     * POST /transToBanks
     *
     * @param Createtrans_to_bankAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtrans_to_bankAPIRequest $request)
    {
        $input = $request->all();
        $user_id = getLoggedInUserId();
        $input['user_id'] = $user_id;
        if($request->file('paper_photo')!= null){
            // print_r("Hello");
            $save_path  = self::file_upload($request->file('paper_photo'));

            $input['paper_photo'] = $save_path;
        }

        $transToBank = $this->transToBankRepository->create($input);

        return $this->sendResponse(new trans_to_bankResource($transToBank), 'Trans To Bank saved successfully');
    }

    public function file_upload($file)
    {   
        $dir_name = "trans_req";
        $head = "/uploads/";
        $pat = Storage::put($dir_name, $file);
        return $head.$pat;

    }
    /**
     * Display the specified trans_to_bank.
     * GET|HEAD /transToBanks/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var trans_to_bank $transToBank */
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            return $this->sendError('Trans To Bank not found');
        }

        return $this->sendResponse(new trans_to_bankResource($transToBank), 'Trans To Bank retrieved successfully');
    }

    /**
     * Update the specified trans_to_bank in storage.
     * PUT/PATCH /transToBanks/{id}
     *
     * @param int $id
     * @param Updatetrans_to_bankAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetrans_to_bankAPIRequest $request)
    {
        $input = $request->all();

        /** @var trans_to_bank $transToBank */
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            return $this->sendError('Trans To Bank not found');
        }

        $transToBank = $this->transToBankRepository->update($input, $id);

        return $this->sendResponse(new trans_to_bankResource($transToBank), 'trans_to_bank updated successfully');
    }

    /**
     * Remove the specified trans_to_bank from storage.
     * DELETE /transToBanks/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var trans_to_bank $transToBank */
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            return $this->sendError('Trans To Bank not found');
        }

        $transToBank->delete();

        return $this->sendSuccess('Trans To Bank deleted successfully');
    }
}
