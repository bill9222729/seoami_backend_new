<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createslider_managerAPIRequest;
use App\Http\Requests\API\Updateslider_managerAPIRequest;
use App\Models\slider_manager;
use App\Repositories\slider_managerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\slider_managerResource;
use Response;

/**
 * Class slider_managerController
 * @package App\Http\Controllers\API
 */

class slider_managerAPIController extends AppBaseController
{
    /** @var  slider_managerRepository */
    private $sliderManagerRepository;

    public function __construct(slider_managerRepository $sliderManagerRepo)
    {
        $this->sliderManagerRepository = $sliderManagerRepo;
    }

    /**
     * Display a listing of the slider_manager.
     * GET|HEAD /sliderManagers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sliderManagers = $this->sliderManagerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(slider_managerResource::collection($sliderManagers), 'Slider Managers retrieved successfully');
    }

    /**
     * Store a newly created slider_manager in storage.
     * POST /sliderManagers
     *
     * @param Createslider_managerAPIRequest $request
     *
     * @return Response
     */
    public function store(Createslider_managerAPIRequest $request)
    {
        $input = $request->all();

        $sliderManager = $this->sliderManagerRepository->create($input);

        return $this->sendResponse(new slider_managerResource($sliderManager), 'Slider Manager saved successfully');
    }

    /**
     * Display the specified slider_manager.
     * GET|HEAD /sliderManagers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var slider_manager $sliderManager */
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            return $this->sendError('Slider Manager not found');
        }

        return $this->sendResponse(new slider_managerResource($sliderManager), 'Slider Manager retrieved successfully');
    }

    /**
     * Update the specified slider_manager in storage.
     * PUT/PATCH /sliderManagers/{id}
     *
     * @param int $id
     * @param Updateslider_managerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateslider_managerAPIRequest $request)
    {
        $input = $request->all();

        /** @var slider_manager $sliderManager */
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            return $this->sendError('Slider Manager not found');
        }

        $sliderManager = $this->sliderManagerRepository->update($input, $id);

        return $this->sendResponse(new slider_managerResource($sliderManager), 'slider_manager updated successfully');
    }

    /**
     * Remove the specified slider_manager from storage.
     * DELETE /sliderManagers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var slider_manager $sliderManager */
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            return $this->sendError('Slider Manager not found');
        }

        $sliderManager->delete();

        return $this->sendSuccess('Slider Manager deleted successfully');
    }
}
