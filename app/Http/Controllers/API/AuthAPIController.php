<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\RegisterRequest;
use App\Models\Role;
use App\Models\wallet;
use App\Models\User;
use App\Models\seo_thread_manager;
use App\Repositories\AccountRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Crypt;
use Exception;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use InfyOm\Generator\Utils\ResponseUtil;
use App\Http\Controllers\API\seo_thread_managerAPIController;
class AuthAPIController extends AppBaseController
{
    /** @var AccountRepository */
    public $accountRepo;
    /** @var UserRepository */
    private $userRepository;

    public function __construct(AccountRepository $accountRepository, UserRepository $userRepo)
    {
        $this->accountRepo = $accountRepository;
        $this->userRepository = $userRepo;
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (empty($email) or empty($password)) {
            return $this->sendError('username and password required', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var User $user */
        $user = User::whereRaw('lower(email) = ?', [$email])->first();
        if (empty($user)) {
            return $this->sendError('Invalid username or password', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (! Hash::check($password, $user->password)) {
            return $this->sendError('Invalid username or password', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (! $user->is_active) {
            
            $data = json_encode(ResponseUtil::makeError('Your account is deactivated. Please verify your email for account activation.'));
            return JsonResponse::fromJsonString($data, Response::HTTP_UNAUTHORIZED);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        $user->update(['is_online' => 1, 'last_seen' => null]);

        return $this->sendResponse(['token' => $tokenResult->accessToken, 'user' => $user], 'Logged in successfully.');
    }

    /**
     * @param  RegisterRequest  $request
     *
     * @throws \Exception
     * @return JsonResponse
     */ 
    public function register(RegisterRequest $request)
    {
        
        // echo 123;
        // dd($request->all());
        // return $this->sendSuccess('Please check your email inbox to confirm your account. Do not forget to check your spam folder as well.');
        $input = $request->all();
        // dd($input);
        /** @var User $user */
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'phone'=> $input['phone'],
            'password' => Hash::make($input['password']),
            'email_verified_at'=>Carbon::now(),
            'language'=>'zh',
            'is_active' =>1,
            'privacy'=>0
        ]);

        $apiUrl = url('/api');

        $this->userRepository->assignRoles($user, ['role_id' => Role::MEMBER_ROLE]);
        $activateCode = $this->accountRepo->generateUserActivationToken($user->id);

        //創建錢包
        $wa = new Wallet();
        $wa->user_id = $user->id;
        $wa->point = 0;
        $wa->save();

        //創建線程
        try {
            seo_thread_managerAPIController::Create_All_User_Seo_Thread();
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            // print_r($th);
        }
        
        // $this->accountRepo->sendConfirmEmail($user->name, $user->email, $activateCode, $apiUrl);

        return $this->sendSuccess('Please check your email inbox to confirm your account. Do not forget to check your spam folder as well.');
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        $authUser = getLoggedInUser();
        $userTokens = $authUser->tokens;

        foreach ($userTokens as $token) {
            /** var Laravel\Passport\Token $token */
            $token->revoke();
        }

        $authUser->update(['is_online' => 0, 'last_seen' => Carbon::now()]);

        return $this->sendSuccess('Logged out successfully.');
    }

    public function verifyAccount(Request $request)
    {
        $token = $request->get('token', null);
        $baseUrl = url('/');

        if (empty($token)) {
            return redirect('/login?success=0&msg=token not found.');
        }

        try {
            $token = Crypt::decrypt($token);
            list($userId, $activationCode) = $result = explode('|', $token);
            $loginUrl = $baseUrl.'/login';

            if (count($result) < 2) {
                return redirect($loginUrl.'?success=0&msg=token not found.');
            }

            /** @var User $user */
            $user = User::whereActivationCode($activationCode)->findOrFail($userId);

            if (empty($user)) {
                return redirect($loginUrl.'?success=0&msg=This account activation token is invalid.');
            }
            if ($user->is_active) {
                return redirect($loginUrl.'?success=0&msg=Your account already activated. Please do a login.');
            }

            $user->is_active = 1;
            $user->save();

            return redirect($loginUrl.'?success=1&msg=Your account is successfully activated. Please do a login.');
        } catch (Exception $e) {
            return redirect('/login?success=0&msg=Something went wrong.');
        }
    }
}
