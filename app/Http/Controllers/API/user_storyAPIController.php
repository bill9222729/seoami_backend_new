<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createuser_storyAPIRequest;
use App\Http\Requests\API\Updateuser_storyAPIRequest;
use App\Models\user_story;
use App\Models\User;
use App\Repositories\user_storyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\user_storyResource;
use Response;
use App\Models\slider_manager;
use Storage;
use Auth;
/**
 * Class user_storyController
 * @package App\Http\Controllers\API
 */

class user_storyAPIController extends AppBaseController
{
    /** @var  user_storyRepository */
    private $userStoryRepository;

    public function __construct(user_storyRepository $userStoryRepo)
    {
        $this->userStoryRepository = $userStoryRepo;
    }
    public function add_story_count($id){
        $story = user_story::find($id);
      
        if($story == null){
            return "0";
        }else{
            $story->views +=1;
            $story->save();
            return $story->views;
        }
    }
    /**
     * Display a listing of the user_story.
     * GET|HEAD /userStories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $userStories = $this->userStoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $userStory = $userStories->toArray();
        $end = [];
     
        foreach ($userStory as $key => $value) {
         
           $user_data = User::find($value['user_id']);
         
           $value['user_data'] = $user_data;
           $value['data'] = config('app.url').$value['data'];
           $end[] = $value;
        }
        // dd($end);
        return $this->sendResponse($end, 'User Stories retrieved successfully');
    }
    public function upload_storys(Request $request){
        $user_id = Auth::id();
        $input = $request->all();
        // print_r(gettype($input['data']));
        if($request->file('data')!= null){
            // print_r("Hello");
            $save_path  = self::file_upload($request->file('data'));
// dd($save_path);
            // $request->Logo = $save_path;
            $input['data'] = $save_path;
        }

        $input['user_id'] = $user_id;
        $input['background'] = "";

        
        $userStory = $this->userStoryRepository->create($input);
        // print_r($request->file('data'));
        
        return $this->sendResponse(new user_storyResource($userStory), 'User Story saved successfully');

    }
    public function file_upload($file)
    {   
        $dir_name = "story";
        $head = "/uploads/";
        $pat = Storage::put($dir_name, $file);
        return $head.$pat;

    }
    public function get_home_msg(){
        $slider = slider_manager::all();
        $user_story = user_story::all();
        $end = [];
        $end['slider'] = $slider;
        $end['user_story'] = $user_story;
        return $this->sendResponse($end, 'User Stories retrieved successfully');


    }
    /**
     * Store a newly created user_story in storage.
     * POST /userStories
     *
     * @param Createuser_storyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createuser_storyAPIRequest $request)
    {
        $input = $request->all();

        $userStory = $this->userStoryRepository->create($input);

        return $this->sendResponse(new user_storyResource($userStory), 'User Story saved successfully');
    }

    /**
     * Display the specified user_story.
     * GET|HEAD /userStories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var user_story $userStory */
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            return $this->sendError('User Story not found');
        }
      
        return $this->sendResponse(new user_storyResource($userStory), 'User Story retrieved successfully');
    }

    /**
     * Update the specified user_story in storage.
     * PUT/PATCH /userStories/{id}
     *
     * @param int $id
     * @param Updateuser_storyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateuser_storyAPIRequest $request)
    {
        $input = $request->all();

        /** @var user_story $userStory */
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            return $this->sendError('User Story not found');
        }

        $userStory = $this->userStoryRepository->update($input, $id);

        return $this->sendResponse(new user_storyResource($userStory), 'user_story updated successfully');
    }

    /**
     * Remove the specified user_story from storage.
     * DELETE /userStories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var user_story $userStory */
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            return $this->sendError('User Story not found');
        }

        $userStory->delete();

        return $this->sendSuccess('User Story deleted successfully');
    }
}
