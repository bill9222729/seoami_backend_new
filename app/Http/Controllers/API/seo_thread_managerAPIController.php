<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Auth;
use App\Http\Requests\API\Createseo_thread_managerAPIRequest;
use App\Http\Requests\API\Updateseo_thread_managerAPIRequest;
use App\Models\seo_thread_manager;
use App\Models\wallet;
use App\Models\User;
use App\Repositories\seo_thread_managerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\seo_thread_managerResource;
use Response;
use App\Http\Controllers\API\walletAPIController;
/**
 * Class seo_thread_managerController
 * @package App\Http\Controllers\API
 */

class seo_thread_managerAPIController extends AppBaseController
{
    /** @var  seo_thread_managerRepository */
    private $seoThreadManagerRepository;

    public function __construct(seo_thread_managerRepository $seoThreadManagerRepo)
    {
        $this->seoThreadManagerRepository = $seoThreadManagerRepo;
    }

    public function buy_seo_thread(Request $request){
        $from_id =  getLoggedInUserId();
        $recv_id = 1;
        $reason = "購買SEO線程";
        $amount = $request->num * 330;
        $stat = walletAPIController:: Trans_fn($from_id,$recv_id,$amount,$reason);
        $stat = json_decode($stat,1);
        // print_r($stat);
        error_log(print_r($stat, true));
        if($stat['status']==false){
            return 0;
        }

        // print_r($stat);
        $exp_time = Carbon::now()->addYear();
        $new_thread = self::Gen_Thread_Config("pay",$exp_time);
        $user_id = $from_id;
        $mission_zip = seo_thread_manager::where('user_id',$user_id)->first();
        $pay_thread = $mission_zip->payment_thread;
        $pay_thread = json_decode($pay_thread,1);
        $pay_thread[] = $new_thread;
        $pay_thread = json_encode($pay_thread);
        $mission_zip->payment_thread = $pay_thread;
        $mission_zip->save();
        return 1;
    }
    public function seo_mission_update(Request $request){

        $mission_zip = seo_thread_manager::where('user_id',$request->user_id)->first();

        $thread = $mission_zip->free_thread;
        if($request->type == 'pay'){
            $thread = $mission_zip->payment_thread;
        }

        $thread = json_decode($thread,1);
        // echo "123";
        // return $thread[$request->num] ;
        
        $thread[$request->num]['views'] =  $thread[$request->num]['views'] + 1;
      
        
        // return $thread[$request->num] ;
        $thread = json_encode($thread);
        if($request->type == 'pay'){
            $mission_zip->payment_thread = $thread;
        }
        if($request->type == 'free'){
            $mission_zip->free_thread = $thread;
        }
        
        $mission_zip->save();
    }
    public function seo_worker(){
        $all_mission = seo_thread_manager::all();
    
        $all_url = [];
        foreach ($all_mission as $key => $value) {
        
            $free_thread = $value->free_thread;
            $free_thread = json_decode($free_thread);
            $pay_thread = $value->payment_thread;
            $pay_thread = json_decode($pay_thread);
            // if($value->id == 36){
            //     dd($free_thread);
            // }
          try {
            if($pay_thread != null){
            foreach ($pay_thread as $pk => $p_t) {
                if($p_t->url != null){
                $tt = ['type'=>"pay","num"=>$pk,"url"=>$p_t->url,'user_id'=>$value->user_id];
                 $all_url[] = $tt;
                }
             }

          
            }
            if($free_thread != null){
            foreach ($free_thread as $pk => $p_t) {
               
                if($p_t->url != null){
                    $tt = ['type'=>"free","num"=>$pk,"url"=>$p_t->url,'user_id'=>$value->user_id];
                 $all_url[] = $tt;
                 
                }
             }
            }
          } catch (\Throwable $th) {
            // dd($pay_thread);
          }
           
           
        }
        shuffle($all_url);
        return $all_url;
    }
    public static function Gen_Thread_Config($thread_type,$exp_time=null){
        $exp = Carbon::parse("2100/11/15");
        $pack = [];
        $pack['url'] = null;
        $pack['types'] = $thread_type;
        
        if($thread_type !='free'){
            $exp = Carbon::parse($exp_time);
        }

        $pack['exp'] = $exp->timestamp;
        $pack['views'] = 0;
        $pack['exp_date'] = $exp->format('Y-m-d');
        return $pack;

    }
    public static function Create_All_User_Seo_Thread(){
        $all_user = User::all();
        foreach ($all_user as $key => $value) {
          
        $now_thread = seo_thread_manager::where('user_id',$value->id)->first();
        if($now_thread == null){
            $new_thread = new seo_thread_manager();
            $new_thread->user_id = $value->id;
            $free_t = [];
            $open_n = 3;
            if($value->lv == "diamond"){
                $open_n = 9;
            }
            if($value->lv == "platinum"){
                $open_n = 6;
            }
            
            for ($i=0; $i < $open_n; $i++) { 
                $free_t[] = self::Gen_Thread_Config("free");
            }
            $new_thread->free_thread = json_encode($free_t);
            $new_thread->payment_thread = json_encode([]);
            $new_thread ->save();
            
        }
        }

    }

    /**
     * Display a listing of the seo_thread_manager.
     * GET|HEAD /seoThreadManagers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        
        $user_id = getLoggedInUserId();
       
        $seoThreadManagers = $this->seoThreadManagerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        )->where('user_id',$user_id);
        
        return $this->sendResponse(seo_thread_managerResource::collection($seoThreadManagers), 'Seo Thread Managers retrieved successfully');
    }
  
    public function seo_thread_edit(Request $request){
        
        
        $user_id = getLoggedInUserId();
        $thread =seo_thread_manager ::where('user_id',$user_id)->first();
        if($request->type == 'free'){
            $edit_thread = $thread->free_thread;

        }else{
            $edit_thread = $thread->payment_thread;
        }
        
        $new_thread = json_decode($edit_thread,1);
        $num = $request->num;
        $new_thread[$num]['url'] = $request->url;
        $new_thread[$num]['views'] =0;
        
        $new_thread = json_encode($new_thread);
        if($request->type == 'free'){
            $thread->free_thread = $new_thread;

        }else{
            $thread->payment_thread = $new_thread;

        }
        $thread->save();

    }
    /**
     * Store a newly created seo_thread_manager in storage.
     * POST /seoThreadManagers
     *
     * @param Createseo_thread_managerAPIRequest $request
     *
     * @return Response
     */
    public function store(Createseo_thread_managerAPIRequest $request)
    {
        $input = $request->all();

        $seoThreadManager = $this->seoThreadManagerRepository->create($input);

        return $this->sendResponse(new seo_thread_managerResource($seoThreadManager), 'Seo Thread Manager saved successfully');
    }

    /**
     * Display the specified seo_thread_manager.
     * GET|HEAD /seoThreadManagers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var seo_thread_manager $seoThreadManager */
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            return $this->sendError('Seo Thread Manager not found');
        }

        return $this->sendResponse(new seo_thread_managerResource($seoThreadManager), 'Seo Thread Manager retrieved successfully');
    }

    /**
     * Update the specified seo_thread_manager in storage.
     * PUT/PATCH /seoThreadManagers/{id}
     *
     * @param int $id
     * @param Updateseo_thread_managerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateseo_thread_managerAPIRequest $request)
    {
        $input = $request->all();

        /** @var seo_thread_manager $seoThreadManager */
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            return $this->sendError('Seo Thread Manager not found');
        }

        $seoThreadManager = $this->seoThreadManagerRepository->update($input, $id);

        return $this->sendResponse(new seo_thread_managerResource($seoThreadManager), 'seo_thread_manager updated successfully');
    }

    /**
     * Remove the specified seo_thread_manager from storage.
     * DELETE /seoThreadManagers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var seo_thread_manager $seoThreadManager */
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            return $this->sendError('Seo Thread Manager not found');
        }

        $seoThreadManager->delete();

        return $this->sendSuccess('Seo Thread Manager deleted successfully');
    }
}
