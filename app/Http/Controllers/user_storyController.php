<?php

namespace App\Http\Controllers;

use App\DataTables\user_storyDataTable;
use App\Http\Requests;
use App\Http\Requests\Createuser_storyRequest;
use App\Http\Requests\Updateuser_storyRequest;
use App\Repositories\user_storyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class user_storyController extends AppBaseController
{
    /** @var  user_storyRepository */
    private $userStoryRepository;

    public function __construct(user_storyRepository $userStoryRepo)
    {
        $this->userStoryRepository = $userStoryRepo;
    }

    /**
     * Display a listing of the user_story.
     *
     * @param user_storyDataTable $userStoryDataTable
     * @return Response
     */
    public function index(user_storyDataTable $userStoryDataTable)
    {
        return $userStoryDataTable->render('user_stories.index');
    }

    /**
     * Show the form for creating a new user_story.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_stories.create');
    }

    /**
     * Store a newly created user_story in storage.
     *
     * @param Createuser_storyRequest $request
     *
     * @return Response
     */
    public function store(Createuser_storyRequest $request)
    {
        $input = $request->all();

        $userStory = $this->userStoryRepository->create($input);

        Flash::success('User Story saved successfully.');

        return redirect(route('userStories.index'));
    }

    /**
     * Display the specified user_story.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            Flash::error('User Story not found');

            return redirect(route('userStories.index'));
        }

        return view('user_stories.show')->with('userStory', $userStory);
    }

    /**
     * Show the form for editing the specified user_story.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            Flash::error('User Story not found');

            return redirect(route('userStories.index'));
        }

        return view('user_stories.edit')->with('userStory', $userStory);
    }

    /**
     * Update the specified user_story in storage.
     *
     * @param  int              $id
     * @param Updateuser_storyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateuser_storyRequest $request)
    {
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            Flash::error('User Story not found');

            return redirect(route('userStories.index'));
        }

        $userStory = $this->userStoryRepository->update($request->all(), $id);

        Flash::success('User Story updated successfully.');

        return redirect(route('userStories.index'));
    }

    /**
     * Remove the specified user_story from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userStory = $this->userStoryRepository->find($id);

        if (empty($userStory)) {
            Flash::error('User Story not found');

            return redirect(route('userStories.index'));
        }

        $this->userStoryRepository->delete($id);

        Flash::success('User Story deleted successfully.');

        return redirect(route('userStories.index'));
    }
}
