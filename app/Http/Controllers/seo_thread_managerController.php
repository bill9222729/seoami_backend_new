<?php

namespace App\Http\Controllers;

use App\DataTables\seo_thread_managerDataTable;
use App\Http\Requests;
use App\Http\Requests\Createseo_thread_managerRequest;
use App\Http\Requests\Updateseo_thread_managerRequest;
use App\Repositories\seo_thread_managerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class seo_thread_managerController extends AppBaseController
{
    /** @var  seo_thread_managerRepository */
    private $seoThreadManagerRepository;

    public function __construct(seo_thread_managerRepository $seoThreadManagerRepo)
    {
        $this->seoThreadManagerRepository = $seoThreadManagerRepo;
    }

    /**
     * Display a listing of the seo_thread_manager.
     *
     * @param seo_thread_managerDataTable $seoThreadManagerDataTable
     * @return Response
     */
    public function index(seo_thread_managerDataTable $seoThreadManagerDataTable)
    {
        return $seoThreadManagerDataTable->render('seo_thread_managers.index');
    }

    /**
     * Show the form for creating a new seo_thread_manager.
     *
     * @return Response
     */
    public function create()
    {
        return view('seo_thread_managers.create');
    }

    /**
     * Store a newly created seo_thread_manager in storage.
     *
     * @param Createseo_thread_managerRequest $request
     *
     * @return Response
     */
    public function store(Createseo_thread_managerRequest $request)
    {
        $input = $request->all();

        $seoThreadManager = $this->seoThreadManagerRepository->create($input);

        Flash::success('Seo Thread Manager saved successfully.');

        return redirect(route('seoThreadManagers.index'));
    }

    /**
     * Display the specified seo_thread_manager.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            Flash::error('Seo Thread Manager not found');

            return redirect(route('seoThreadManagers.index'));
        }

        return view('seo_thread_managers.show')->with('seoThreadManager', $seoThreadManager);
    }

    /**
     * Show the form for editing the specified seo_thread_manager.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            Flash::error('Seo Thread Manager not found');

            return redirect(route('seoThreadManagers.index'));
        }

        return view('seo_thread_managers.edit')->with('seoThreadManager', $seoThreadManager);
    }

    /**
     * Update the specified seo_thread_manager in storage.
     *
     * @param  int              $id
     * @param Updateseo_thread_managerRequest $request
     *
     * @return Response
     */
    public function update($id, Updateseo_thread_managerRequest $request)
    {
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            Flash::error('Seo Thread Manager not found');

            return redirect(route('seoThreadManagers.index'));
        }

        $seoThreadManager = $this->seoThreadManagerRepository->update($request->all(), $id);

        Flash::success('Seo Thread Manager updated successfully.');

        return redirect(route('seoThreadManagers.index'));
    }

    /**
     * Remove the specified seo_thread_manager from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seoThreadManager = $this->seoThreadManagerRepository->find($id);

        if (empty($seoThreadManager)) {
            Flash::error('Seo Thread Manager not found');

            return redirect(route('seoThreadManagers.index'));
        }

        $this->seoThreadManagerRepository->delete($id);

        Flash::success('Seo Thread Manager deleted successfully.');

        return redirect(route('seoThreadManagers.index'));
    }
}
