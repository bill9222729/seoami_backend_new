<?php

namespace App\Http\Controllers;

use App\DataTables\walletDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatewalletRequest;
use App\Http\Requests\UpdatewalletRequest;
use App\Repositories\walletRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class walletController extends AppBaseController
{
    /** @var  walletRepository */
    private $walletRepository;

    public function __construct(walletRepository $walletRepo)
    {
        $this->walletRepository = $walletRepo;
    }

    /**
     * Display a listing of the wallet.
     *
     * @param walletDataTable $walletDataTable
     * @return Response
     */
    public function index(walletDataTable $walletDataTable)
    {
        return $walletDataTable->render('wallets.index');
    }

    /**
     * Show the form for creating a new wallet.
     *
     * @return Response
     */
    public function create()
    {
        return view('wallets.create');
    }

    /**
     * Store a newly created wallet in storage.
     *
     * @param CreatewalletRequest $request
     *
     * @return Response
     */
    public function store(CreatewalletRequest $request)
    {
        $input = $request->all();

        $wallet = $this->walletRepository->create($input);

        Flash::success('Wallet saved successfully.');

        return redirect(route('wallets.index'));
    }

    /**
     * Display the specified wallet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            Flash::error('Wallet not found');

            return redirect(route('wallets.index'));
        }

        return view('wallets.show')->with('wallet', $wallet);
    }

    /**
     * Show the form for editing the specified wallet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            Flash::error('Wallet not found');

            return redirect(route('wallets.index'));
        }

        return view('wallets.edit')->with('wallet', $wallet);
    }

    /**
     * Update the specified wallet in storage.
     *
     * @param  int              $id
     * @param UpdatewalletRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatewalletRequest $request)
    {
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            Flash::error('Wallet not found');

            return redirect(route('wallets.index'));
        }

        $wallet = $this->walletRepository->update($request->all(), $id);

        Flash::success('Wallet updated successfully.');

        return redirect(route('wallets.index'));
    }

    /**
     * Remove the specified wallet from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wallet = $this->walletRepository->find($id);

        if (empty($wallet)) {
            Flash::error('Wallet not found');

            return redirect(route('wallets.index'));
        }

        $this->walletRepository->delete($id);

        Flash::success('Wallet deleted successfully.');

        return redirect(route('wallets.index'));
    }
}
