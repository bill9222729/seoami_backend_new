<?php

namespace App\Http\Controllers;

use App\DataTables\trans_to_bankDataTable;
use App\Http\Requests;
use App\Http\Requests\Createtrans_to_bankRequest;
use App\Http\Requests\Updatetrans_to_bankRequest;
use App\Repositories\trans_to_bankRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class trans_to_bankController extends AppBaseController
{
    /** @var  trans_to_bankRepository */
    private $transToBankRepository;

    public function __construct(trans_to_bankRepository $transToBankRepo)
    {
        $this->transToBankRepository = $transToBankRepo;
    }

    /**
     * Display a listing of the trans_to_bank.
     *
     * @param trans_to_bankDataTable $transToBankDataTable
     * @return Response
     */
    public function index(trans_to_bankDataTable $transToBankDataTable)
    {
        return $transToBankDataTable->render('trans_to_banks.index');
    }

    /**
     * Show the form for creating a new trans_to_bank.
     *
     * @return Response
     */
    public function create()
    {
        return view('trans_to_banks.create');
    }

    /**
     * Store a newly created trans_to_bank in storage.
     *
     * @param Createtrans_to_bankRequest $request
     *
     * @return Response
     */
    public function store(Createtrans_to_bankRequest $request)
    {
        $input = $request->all();

        $transToBank = $this->transToBankRepository->create($input);

        Flash::success('Trans To Bank saved successfully.');

        return redirect(route('transToBanks.index'));
    }

    /**
     * Display the specified trans_to_bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            Flash::error('Trans To Bank not found');

            return redirect(route('transToBanks.index'));
        }

        return view('trans_to_banks.show')->with('transToBank', $transToBank);
    }

    /**
     * Show the form for editing the specified trans_to_bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            Flash::error('Trans To Bank not found');

            return redirect(route('transToBanks.index'));
        }

        return view('trans_to_banks.edit')->with('transToBank', $transToBank);
    }

    /**
     * Update the specified trans_to_bank in storage.
     *
     * @param  int              $id
     * @param Updatetrans_to_bankRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetrans_to_bankRequest $request)
    {
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            Flash::error('Trans To Bank not found');

            return redirect(route('transToBanks.index'));
        }

        $transToBank = $this->transToBankRepository->update($request->all(), $id);

        Flash::success('Trans To Bank updated successfully.');

        return redirect(route('transToBanks.index'));
    }

    /**
     * Remove the specified trans_to_bank from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transToBank = $this->transToBankRepository->find($id);

        if (empty($transToBank)) {
            Flash::error('Trans To Bank not found');

            return redirect(route('transToBanks.index'));
        }

        $this->transToBankRepository->delete($id);

        Flash::success('Trans To Bank deleted successfully.');

        return redirect(route('transToBanks.index'));
    }
}
