<?php

namespace App\Http\Controllers;

use App\DataTables\slider_managerDataTable;
use App\Http\Requests;
use App\Http\Requests\Createslider_managerRequest;
use App\Http\Requests\Updateslider_managerRequest;
use App\Repositories\slider_managerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Storage;
class slider_managerController extends AppBaseController
{
    /** @var  slider_managerRepository */
    private $sliderManagerRepository;

    public function __construct(slider_managerRepository $sliderManagerRepo)
    {
        $this->sliderManagerRepository = $sliderManagerRepo;
    }

    /**
     * Display a listing of the slider_manager.
     *
     * @param slider_managerDataTable $sliderManagerDataTable
     * @return Response
     */
    public function index(slider_managerDataTable $sliderManagerDataTable)
    {
        return $sliderManagerDataTable->render('slider_managers.index');
    }

    /**
     * Show the form for creating a new slider_manager.
     *
     * @return Response
     */
    public function create()
    {
        return view('slider_managers.create');
    }

    /**
     * Store a newly created slider_manager in storage.
     *
     * @param Createslider_managerRequest $request
     *
     * @return Response
     */
    public function store(Createslider_managerRequest $request)
    {
        $input = self::file_upload($request, 'path');

        $sliderManager = $this->sliderManagerRepository->create($input);

        Flash::success('Slider Manager saved successfully.');

        return redirect(route('sliderManagers.index'));
    }
    
    /**
     * Display the specified slider_manager.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            Flash::error('Slider Manager not found');

            return redirect(route('sliderManagers.index'));
        }

        return view('slider_managers.show')->with('sliderManager', $sliderManager);
    }

    /**
     * Show the form for editing the specified slider_manager.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            Flash::error('Slider Manager not found');

            return redirect(route('sliderManagers.index'));
        }

        return view('slider_managers.edit')->with('sliderManager', $sliderManager);
    }

    /**
     * Update the specified slider_manager in storage.
     *
     * @param  int              $id
     * @param Updateslider_managerRequest $request
     *
     * @return Response
     */
    public function update($id, Updateslider_managerRequest $request)
    {
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            Flash::error('Slider Manager not found');

            return redirect(route('sliderManagers.index'));
        }

        $input = self::file_upload($request, 'path');

        $sliderManager = $this->sliderManagerRepository->update($input, $id);

        Flash::success('Slider Manager updated successfully.');

        return redirect(route('sliderManagers.index'));
    }

    /**
     * Remove the specified slider_manager from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sliderManager = $this->sliderManagerRepository->find($id);

        if (empty($sliderManager)) {
            Flash::error('Slider Manager not found');

            return redirect(route('sliderManagers.index'));
        }

        $this->sliderManagerRepository->delete($id);

        Flash::success('Slider Manager deleted successfully.');

        return redirect(route('sliderManagers.index'));
    }

    public function file_upload($request, $element){
        $input = $request->all();
        if($request->hasFile($element)){
            $dir_name = "slider_img";
            $head = "/uploads/";
            $pat = Storage::put($dir_name, $request->File($element));
            $input[$element] = $head . $pat;
        }
        return $input;
    }
}
