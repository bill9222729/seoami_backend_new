<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class seo_thread_managerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'free_thread' => $this->free_thread,
            'payment_thread' => $this->payment_thread,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
