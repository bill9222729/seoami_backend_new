<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class wallet_logResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'from_id' => $this->from_id,
            'reason' => $this->reason,
            'point' => $this->point,
            'before_point' => $this->before_point,
            'after_point' => $this->after_point,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
