<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class slider_managerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'path' => $this->path,
            'click_type' => $this->click_type,
            'data' => $this->data,
            'start_date' => $this->start_date,
            'exp_date' => $this->exp_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
