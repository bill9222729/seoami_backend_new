<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class trans_to_bankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'bank_no' => $this->bank_no,
            'bank_name' => $this->bank_name,
            'sub_bank_name' => $this->sub_bank_name,
            'account_name' => $this->account_name,
            'account_no' => $this->account_no,
            'address' => $this->address,
            'contact_name' => $this->contact_name,
            'id_no' => $this->id_no,
            'paper_photo' => $this->paper_photo,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
