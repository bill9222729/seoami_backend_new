<?php

namespace App\Http\Requests\API;

use App\Models\trans_to_bank;
use InfyOm\Generator\Request\APIRequest;

class Createtrans_to_bankAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return trans_to_bank::$rules;
    }
}
