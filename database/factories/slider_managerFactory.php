<?php

namespace Database\Factories;

use App\Models\slider_manager;
use Illuminate\Database\Eloquent\Factories\Factory;

class slider_managerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = slider_manager::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'path' => $this->faker->text,
        'click_type' => $this->faker->word,
        'data' => $this->faker->text,
        'start_date' => $this->faker->date('Y-m-d H:i:s'),
        'exp_date' => $this->faker->date('Y-m-d H:i:s'),
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
