<?php

namespace Database\Factories;

use App\Models\trans_to_bank;
use Illuminate\Database\Eloquent\Factories\Factory;

class trans_to_bankFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = trans_to_bank::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigitNotNull,
        'bank_no' => $this->faker->word,
        'bank_name' => $this->faker->word,
        'sub_bank_name' => $this->faker->word,
        'account_name' => $this->faker->word,
        'account_no' => $this->faker->word,
        'address' => $this->faker->word,
        'contact_name' => $this->faker->word,
        'id_no' => $this->faker->word,
        'paper_photo' => $this->faker->text,
        'status' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
