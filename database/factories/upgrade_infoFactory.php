<?php

namespace Database\Factories;

use App\Models\upgrade_info;
use Illuminate\Database\Eloquent\Factories\Factory;

class upgrade_infoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = upgrade_info::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->word,
        'address' => $this->faker->word,
        'contact_name' => $this->faker->word,
        'email' => $this->faker->word,
        'phone' => $this->faker->word,
        'ref_id' => $this->faker->word,
        'place_id' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
