<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_manager', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('path');
            $table->string('click_type')->default('link');
            $table->text('data');

            $table->timestamp("start_date");
            $table->timestamp("exp_date");
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_manager');
    }
}
