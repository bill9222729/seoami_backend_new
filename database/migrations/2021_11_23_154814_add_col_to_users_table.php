<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('expiry_date')->nullable()->comment('到期日');
            $table->string('business_or_corporate_member')->nullable()->comment('商務/企業會員');
            $table->string('real_name_or_company_name')->nullable()->comment('真實姓名/公司名稱');
            $table->string('first_referee_id')->nullable()->comment('第一推薦人');
            $table->string('second_referee_id')->nullable()->comment('第二推薦人');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('expiry_date');
            $table->dropColumn('business_or_corporate_member');
            $table->dropColumn('real_name_or_company_name');
            $table->dropColumn('first_referee_id');
            $table->dropColumn('second_referee_id');
        });
    }
}
