<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransToBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    

    public function up()
    {
        Schema::create('trans_to_bank', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('bank_no')->nullable();
            $table->string('bank_name');
            $table->string('sub_bank_name');
            $table->string('account_name');
            $table->string('account_no');
            $table->string('address');
            $table->string('contact_name');
            $table->string('id_no');
            $table->text('paper_photo');
            $table->integer('status')->default(0);
            
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_to_bank');
    }
}
