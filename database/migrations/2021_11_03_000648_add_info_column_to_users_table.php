<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('users', function (Blueprint $table) {
            
            $columns = '[
                { "column": "ID", "val": "", "key": "member_no", "editable": false },
                { "column": "統一編號", "val": "", "key": "company_no", "editable": true },
                { "column": "通訊地址", "val": "", "key": "address", "editable": true },
                { "column": "聯絡電話#分機", "val": "", "key": "tel", "editable": true },
                { "column": "聯絡人", "val": "", "key": "contact_name", "editable": true },
                { "column": "部門", "val": "", "key": "part", "editable": true },
                { "column": "職稱", "val": "", "key": "title", "editable": true },
                { "column": "產業描述", "val": "", "key": "desc", "editable": true }
              ]';
              $columns = json_decode($columns,1);
            
              
              foreach ($columns as $key => $value) {
                $table->string($value['key'])->nullable()->comment($value['column']);
              }
          
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
