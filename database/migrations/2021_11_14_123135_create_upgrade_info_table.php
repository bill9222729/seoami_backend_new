<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpgradeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


     

    public function up()
    {
        Schema::create('upgrade_info', function (Blueprint $table) {
            $table->id();
            $table->string('company_name')->comment('公司名稱');
            $table->string('address')->comment('地址')->nullable();
            $table->string('contact_name')->comment('聯絡人名稱');
            $table->string('email')->comment('email');
            $table->string('phone')->comment('手機號碼');
            $table->string('ref_id')->comment('推荐人id');
            $table->string('place_id')->comment('放置人id')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upgrade_info');
    }
}
