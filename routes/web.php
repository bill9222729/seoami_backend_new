<?php

use App\Http\Controllers\API;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\ReportUserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\UserController;
use Database\Seeders\CreatePermissionSeeder;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('upgrade-to-v3-4-0', function () {
    try {
        \Artisan::call('migrate', [
            '--path'  => '/database/migrations/2020_10_19_133700_move_all_existing_devices_to_new_table.php',
            '--force' => true,
        ]);

        return 'You are successfully migrated to v3.4.0';
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
});

Route::get('/upgrade-to-v4-3-0', function () {
    try {
        Artisan::call('db:seed', ['--class' => 'CreatePermissionSeeder']);

        return 'You are successfully seeded to v4.3.0';
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
});

Route::get('upgrade-to-v5-0-0', function () {
    try {
        \Artisan::call('migrate', [
            '--path'  => '/database/migrations/2021_07_12_000000_add_uuid_to_failed_jobs_table.php',
            '--force' => true,
        ]);

        return 'You are successfully migrated to v5.0.0';
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
});

Auth::routes();
Route::get('activate', [AuthController::class, 'verifyAccount']);

Route::get('/home', [HomeController::class, 'index']);
Route::post('update-language', [UserController::class, 'updateLanguage'])->middleware('auth');

// Impersonate Logout
Route::get('/users/impersonate-logout', [UserController::class, 'userImpersonateLogout'])->name('impersonate.userLogout');

Route::group(['middleware' => ['user.activated', 'auth']], function () {
    //view routes
    Route::get('/conversations',
        [ChatController::class, 'index'])->name('conversations')->middleware('permission:manage_conversations');
    Route::get('profile', [UserController::class, 'getProfile']);
    Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

    //get all user list for chat
    Route::get('users-list', [API\UserAPIController::class, 'getUsersList']);
    Route::get('get-users', [API\UserAPIController::class, 'getUsers']);
    Route::delete('remove-profile-image', [API\UserAPIController::class, 'removeProfileImage']);
    /** Change password */
    Route::post('change-password', [API\UserAPIController::class, 'changePassword']);
    Route::get('conversations/{ownerId}/archive-chat', [API\UserAPIController::class, 'archiveChat']);

    Route::get('get-profile', [API\UserAPIController::class, 'getProfile']);
    Route::post('profile', [API\UserAPIController::class, 'updateProfile'])->name('update.profile');
    Route::post('update-last-seen', [API\UserAPIController::class, 'updateLastSeen']);

    Route::post('send-message',
        [API\ChatAPIController::class, 'sendMessage'])->name('conversations.store')->middleware('sendMessage');
    Route::get('users/{id}/conversation', [API\UserAPIController::class, 'getConversation']);
    Route::get('conversations-list', [API\ChatAPIController::class, 'getLatestConversations']);
    Route::get('archive-conversations', [API\ChatAPIController::class, 'getArchiveConversations']);
    Route::post('read-message', [API\ChatAPIController::class, 'updateConversationStatus']);
    Route::post('file-upload', [API\ChatAPIController::class, 'addAttachment'])->name('file-upload');
    Route::post('image-upload', [API\ChatAPIController::class, 'imageUpload'])->name('image-upload');
    Route::get('conversations/{userId}/delete', [API\ChatAPIController::class, 'deleteConversation']);
    Route::post('conversations/message/{conversation}/delete', [API\ChatAPIController::class, 'deleteMessage']);
    Route::post('conversations/{conversation}/delete', [API\ChatAPIController::class, 'deleteMessageForEveryone']);
    Route::get('/conversations/{conversation}', [API\ChatAPIController::class, 'show']);
    Route::post('send-chat-request', [API\ChatAPIController::class, 'sendChatRequest'])->name('send-chat-request');
    Route::post('accept-chat-request',
        [API\ChatAPIController::class, 'acceptChatRequest'])->name('accept-chat-request');
    Route::post('decline-chat-request',
        [API\ChatAPIController::class, 'declineChatRequest'])->name('decline-chat-request');

    /** Web Notifications */
    Route::put('update-web-notifications', [API\UserAPIController::class, 'updateNotification']);

    /** BLock-Unblock User */
    Route::put('users/{user}/block-unblock', [API\BlockUserAPIController::class, 'blockUnblockUser']);
    Route::get('blocked-users', [API\BlockUserAPIController::class, 'blockedUsers']);

    /** My Contacts */
    Route::get('my-contacts', [API\UserAPIController::class, 'myContacts'])->name('my-contacts');

    /** Groups API */
    Route::post('groups', [API\GroupAPIController::class, 'create']);
    Route::post('groups/{group}', [API\GroupAPIController::class, 'update']);
    Route::get('groups', [API\GroupAPIController::class, 'index']);
    Route::get('groups/{group}', [API\GroupAPIController::class, 'show']);
    Route::put('groups/{group}/add-members', [API\GroupAPIController::class, 'addMembers']);
    Route::delete('groups/{group}/members/{user}', [API\GroupAPIController::class, 'removeMemberFromGroup']);
    Route::delete('groups/{group}/leave', [API\GroupAPIController::class, 'leaveGroup']);
    Route::delete('groups/{group}/remove', [API\GroupAPIController::class, 'removeGroup']);
    Route::put('groups/{group}/members/{user}/make-admin', [API\GroupAPIController::class, 'makeAdmin']);
    Route::put('groups/{group}/members/{user}/dismiss-as-admin', [API\GroupAPIController::class, 'dismissAsAdmin']);
    Route::get('users-blocked-by-me', [API\BlockUserAPIController::class, 'blockUsersByMe']);

    Route::get('notification/{notification}/read', [API\NotificationController::class, 'readNotification']);
    Route::get('notification/read-all', [API\NotificationController::class, 'readAllNotification']);

    /** Web Notifications */
    Route::put('update-web-notifications', [API\UserAPIController::class, 'updateNotification']);
    Route::put('update-player-id', [API\UserAPIController::class, 'updatePlayerId']);
    //set user custom status route
    Route::post('set-user-status', [API\UserAPIController::class, 'setUserCustomStatus'])->name('set-user-status');
    Route::get('clear-user-status', [API\UserAPIController::class, 'clearUserCustomStatus'])->name('clear-user-status');

    //report user
    Route::post('report-user', [API\ReportUserController::class, 'store'])->name('report-user.store');
});

// users
Route::group(['middleware' => ['permission:manage_users', 'auth', 'user.activated']], function () {
    Route::resource('users', UserController::class);
    Route::post('users/{user}/active-de-active', [UserController::class, 'activeDeActiveUser'])
        ->name('active-de-active-user');
    Route::post('users/{user}/update', [UserController::class, 'update']);
    Route::delete('users/{user}/archive', [UserController::class, 'archiveUser']);
    Route::post('users/restore', [UserController::class, 'restoreUser']);
    Route::get('users/{user}/login', [UserController::class, 'userImpersonateLogin']);
    Route::post('users/{user}/email-verified', [UserController::class, 'isEmailVerified'])->name('user.email-verified');
});

// roles
Route::group(['middleware' => ['permission:manage_roles', 'auth', 'user.activated']], function () {
    Route::resource('roles', RoleController::class)->except('update');
    Route::post('roles/{role}/update', [RoleController::class, 'update'])->name('roles.update');
});

// settings
Route::group(['middleware' => ['permission:manage_settings', 'auth', 'user.activated']], function () {
    Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
    Route::post('settings', [SettingsController::class, 'update'])->name('settings.update');
});

// reported-users
Route::group(['middleware' => ['permission:manage_reported_users', 'auth', 'user.activated']], function () {
    Route::resource('reported-users', ReportUserController::class);
});

// meetings
Route::group(['middleware' => ['permission:manage_meetings', 'auth', 'user.activated']], function () {
    Route::resource('meetings', MeetingController::class);
    Route::get('meetings/{meeting}/change-status', [MeetingController::class, 'changeMeetingStatus']);
});

Route::group(['middleware' => ['permission:manage_meetings', 'auth', 'user.activated']], function () {
    Route::get('member/meetings', [MeetingController::class, 'showMemberMeetings'])->name('meetings.member_index');
});

Route::group(['middleware' => ['web']], function () {
    Route::get('login/{provider}', [\App\Http\Controllers\Auth\SocialAuthController::class, 'redirect']);
    Route::get('login/{provider}/callback', [\App\Http\Controllers\Auth\SocialAuthController::class, 'callback']);
});


Route::resource('sliderManagers', App\Http\Controllers\slider_managerController::class);


Route::resource('userStories', App\Http\Controllers\user_storyController::class);


Route::resource('userStories', App\Http\Controllers\user_storyController::class);


Route::resource('upgradeInfos', App\Http\Controllers\upgrade_infoController::class);


Route::resource('walletLogs', App\Http\Controllers\wallet_logController::class);


Route::resource('wallets', App\Http\Controllers\walletController::class);


Route::resource('transToBanks', App\Http\Controllers\trans_to_bankController::class);


Route::resource('seoThreadManagers', App\Http\Controllers\seo_thread_managerController::class);
