<?php

use App\Http\Controllers\API\AdminUsersAPIController;
use App\Http\Controllers\API\AuthAPIController;
use App\Http\Controllers\API\ChatAPIController;
use App\Http\Controllers\API\GroupAPIController;
use App\Http\Controllers\API\PasswordResetController;
use App\Http\Controllers\API\RoleAPIController;
use App\Http\Controllers\API\SocialAuthAPIController;
use App\Http\Controllers\API\UserAPIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User Login API
Route::post('/login', [AuthAPIController::class, 'login']);
Route::post('/register', [AuthAPIController::class, 'register']);
Route::post('password/reset', [PasswordResetController::class, 'sendResetPasswordLink']);
Route::post('password/update', [PasswordResetController::class, 'reset']);
Route::get('activate', [AuthAPIController::class, 'verifyAccount']);
Route::post('import_data', [AdminUsersAPIController::class, 'import_data']);


Route::get('profile/{id}', [UserAPIController::class, 'Get_Other_Profile']);
Route::get('pushmsg', [ChatAPIController::class, 'pusher']);

Route::group(['middleware' => ['auth:api', 'user.activated']], function () {
    Route::post('broadcasting/auth', [\Illuminate\Broadcasting\BroadcastController::class, 'authenticate']);
    Route::get('logout', [AuthAPIController::class, 'logout']);
    Route::get('/conversations/{conversation}', [ChatAPIController::class, 'show']);
    //get all user list for chat
    Route::get('users-list', [UserAPIController::class, 'getUsersList']);
    Route::post('change-password', [UserAPIController::class, 'changePassword']);

    Route::get('my-contacts', [UserAPIController::class, 'myContacts'])->name('my-contacts');
    Route::get('profile', [UserAPIController::class, 'getProfile'])->name('my-profile');
  
    Route::post('profile', [UserAPIController::class, 'updateProfile']);
    Route::post('update-last-seen', [UserAPIController::class, 'updateLastSeen']);
    Route::post('send-chat-request', [ChatAPIController::class, 'sendChatRequest'])->name('send-chat-request');
    Route::post('accept-chat-request',
    [ChatAPIController::class, 'acceptChatRequest'])->name('accept-chat-request');

    
    Route::post('profile_upload_img', [UserAPIController::class, 'profile_upload_img']);
    Route::post('send-message', [ChatAPIController::class, 'sendMessage'])->name('conversations.store');
    Route::post('send-photo', [ChatAPIController::class, 'imageUpload_APP']);
    Route::get('users/{id}/conversation', [UserAPIController::class, 'getConversation']);
    Route::get('conversations', [ChatAPIController::class, 'getLatestConversations']);
    Route::get('my-chat-request', [ChatAPIController::class, 'getChatRequest']);
    Route::post('read-message', [ChatAPIController::class, 'updateConversationStatus']);
    Route::post('file-upload', [ChatAPIController::class, 'addAttachment'])->name('file-upload');
    Route::get('conversations/{userId}/delete', [ChatAPIController::class, 'deleteConversation']);


    /** Update Web-push */
    Route::put('update-web-notifications', [UserAPIController::class, 'updateNotification']);

    /** create group **/
    Route::post('groups', [GroupAPIController::class, 'create'])->name('create-group');
    /** Social Login */
    Route::post('social-login/{provider}', [SocialAuthAPIController::class, 'socialLogin'])->middleware('web');

    Route::post('post_user_story', [ App\Http\Controllers\API\user_storyAPIController::class, 'upload_storys']);
    Route::resource('users', AdminUsersAPIController::class);
    
    
Route::resource('user_stories', App\Http\Controllers\API\user_storyAPIController::class);


Route::post('create_upgrade', [App\Http\Controllers\API\upgrade_infoAPIController::class, 'create_upgrade']);

Route::post('trans',[App\Http\Controllers\API\walletAPIController::class,'Trans']);
Route::get('my-wallet', [App\Http\Controllers\API\walletAPIController::class, 'my_wallet']);


Route::resource('trans_to_banks', App\Http\Controllers\API\trans_to_bankAPIController::class);


Route::resource('seo_thread_managers', App\Http\Controllers\API\seo_thread_managerAPIController::class);
Route::post('seo_thread_edit',[App\Http\Controllers\API\seo_thread_managerAPIController::class,'seo_thread_edit']);

Route::get('seo_worker',[App\Http\Controllers\API\seo_thread_managerAPIController::class,'seo_worker']);
Route::post('seo_mission_update',[App\Http\Controllers\API\seo_thread_managerAPIController::class,'seo_mission_update']);
Route::post('buy_seo_thread',[App\Http\Controllers\API\seo_thread_managerAPIController::class,'buy_seo_thread']);

Route::post('reg_device_token', [UserAPIController::class, 'reg_device_token']);


Route::get('add-story-count/{id}',[App\Http\Controllers\API\user_storyAPIController::class,'add_story_count']);

Route::get('is-friend/{id}',[UserAPIController::class,'check_is_friend']);

});
// Route::get('reg_device_token', [UserAPIController::class, 'reg_device_token']);

Route::group(['middleware' => ['role:Admin', 'auth:api', 'user.activated']], function () {
    Route::resource('users', AdminUsersAPIController::class);
    Route::post('users/{user}/update', [AdminUsersAPIController::class, 'update']);
    Route::post('users/{user}/active-de-active', [AdminUsersAPIController::class, 'activeDeActiveUser'])
        ->name('active-de-active-user');

    Route::resource('roles', RoleAPIController::class);
    Route::post('roles/{role}/update', [RoleAPIController::class, 'update']);


});

    
Route::resource('upgrade_infos', App\Http\Controllers\API\upgrade_infoAPIController::class);

Route::resource('slider_managers', App\Http\Controllers\API\slider_managerAPIController::class);




Route::get('get_home_msg', [App\Http\Controllers\API\user_storyAPIController::class,'get_home_msg']);


Route::resource('wallet_logs', App\Http\Controllers\API\wallet_logAPIController::class);


Route::resource('wallets', App\Http\Controllers\API\walletAPIController::class);


Route::get('wa/init', [App\Http\Controllers\API\walletAPIController::class, 'Create_All_User_Wallet']);
Route::get('seo/init', [App\Http\Controllers\API\seo_thread_managerAPIController::class, 'Create_All_User_Seo_Thread']);
 